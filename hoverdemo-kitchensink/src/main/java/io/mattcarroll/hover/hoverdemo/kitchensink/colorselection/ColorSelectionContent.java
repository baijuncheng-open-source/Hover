/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.colorselection;

import de.greenrobot.event.EventBus;
import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.Content;
import io.mattcarroll.hover.hoverdemo.kitchensink.ResourceTable;
import io.mattcarroll.hover.hoverdemo.kitchensink.theming.HoverTheme;
import io.mattcarroll.hover.hoverdemo.kitchensink.theming.HoverThemer;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.TabList;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * {@link Content} that displays a color chooser and applies the color selection to the
 * Hover menu UI.
 */
public class ColorSelectionContent extends StackLayout implements Content, Component.BindStateChangedListener {

    private static final int MODE_ACCENT = 0;
    private static final int MODE_BASE = 1;

    private EventBus mBus;
    private HoverThemer mHoverThemer;
    private int mMode;
    private HoverTheme mTheme;
    private TabList mTabLayout;
    //private ColorPicker mColorPicker;
    private Text mAttributionTextView;

    public ColorSelectionContent(@NonNull Context context, @NonNull EventBus bus, @NonNull HoverThemer hoverThemer, @NonNull HoverTheme theme) {
        super(context);
        mBus = bus;
        mHoverThemer = hoverThemer;
        mTheme = theme;
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_color_selection_content,this,true);
        mTabLayout = (TabList) findComponentById(ResourceTable.Id_tablayout);
        //mColorPicker = (ColorPicker) findViewById(R.id.colorpicker);
        mAttributionTextView = (Text) findComponentById(ResourceTable.Id_textview_attribution);

        TabList.Tab tab = mTabLayout.new Tab(getContext());
        tab.setText("Accent Color");
        mTabLayout.addTab(tab);

        TabList.Tab tab2 = mTabLayout.new Tab(getContext());
        tab2.setText("Primary Color");
        mTabLayout.addTab(tab2);
        mTabLayout.setTabMargin(20);
        mTabLayout.setTabLength(500);
        mTabLayout.setTabTextSize(50);

        updateView();
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        mBus.register(this);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mBus.unregister(this);
    }

    @NonNull
    @Override
    public Component getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {

    }

    @Override
    public void onHidden() {

    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mTheme = newTheme;
        updateView();
    }

    private void updateView() {
        mTabLayout.setSelectedTabIndicatorColor(mTheme.getAccentColor());
        mTabLayout.setTabTextColors(0xFFCCCCCC, mTheme.getAccentColor());
        mAttributionTextView.setTextColor(new Color(mTheme.getAccentColor()));
    }
}
