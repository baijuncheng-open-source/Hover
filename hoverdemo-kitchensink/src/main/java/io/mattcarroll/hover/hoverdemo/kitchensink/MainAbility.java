package io.mattcarroll.hover.hoverdemo.kitchensink;

import io.mattcarroll.hover.hoverdemo.kitchensink.slice.MainAbilitySlice;
import io.mattcarroll.hover.overlay.OverlayPermission;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.sysappcomponents.settings.AppSettings;

public class MainAbility extends Ability {


    private static final int REQUEST_CODE_HOVER_PERMISSION = 1000;

    private Button mButton_launch_menu;

    private Button mButton_launch_hover_menu_activity;

    private Button mButton_launch_activity;

    private boolean mPermissionsRequested = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);


        mButton_launch_menu = (Button) findComponentById(ResourceTable.Id_button_launch_menu);
        mButton_launch_hover_menu_activity = (Button) findComponentById(ResourceTable.Id_button_launch_hover_menu_activity);
        mButton_launch_activity = (Button) findComponentById(ResourceTable.Id_button_launch_activity);


        mButton_launch_menu.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!checkPermission()) {
                    return;
                }
                DemoHoverMenuService.showFloatingMenu(MainAbility.this);
            }
        });

        mButton_launch_hover_menu_activity.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!checkPermission()) {
                    return;
                }
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("io.mattcarroll.hover.hoverdemo.kitchensink")
                        .withAbilityName("io.mattcarroll.hover.hoverdemo.kitchensink.DemoHoverMenuActivity")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });

        mButton_launch_activity.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("io.mattcarroll.hover.hoverdemo.kitchensink")
                        .withAbilityName("io.mattcarroll.hover.hoverdemo.kitchensink.MainAbility")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });


        // On M and above we need to ask the user for permission to display the Hover
        // menu within the "alert window" layer.  Use OverlayPermission to check for the permission
        // and to request it.
        if (!mPermissionsRequested && !OverlayPermission.hasRuntimePermissionToDrawOverlay(this)) {
            @SuppressWarnings("NewApi")
            Intent myIntent = OverlayPermission.createIntentToRequestOverlayPermission(this);
            startAbilityForResult(myIntent, REQUEST_CODE_HOVER_PERMISSION);
        }
    }

    private boolean checkPermission() {
        if (AppSettings.canShowFloating(this)) {
            return true;
        }
        Text text = new Text(this);
        text.setTextSize(50);
        text.setMultipleLine(true);
        text.setText(ResourceTable.String_toast_text);
        ToastDialog toastDialog = new ToastDialog(this);
        toastDialog.setComponent(text);
        toastDialog.show();
        return false;
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_CODE_HOVER_PERMISSION == requestCode) {
            mPermissionsRequested = true;
        } else {
            super.onAbilityResult(requestCode, resultCode, data);
        }
    }
}
