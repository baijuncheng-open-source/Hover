/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink;

/**
 * Application class.
 */
//no use code.
//public class App extends Application {
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        setupTheme();
//        setupAppStateTracking();
//    }
//
//    private void setupTheme() {
//        HoverTheme defaultTheme = new HoverTheme(
//                ContextCompat.getColor(this, R.color.hover_accent),
//                ContextCompat.getColor(this, R.color.hover_base));
//        HoverThemeManager.init(Bus.getInstance(), defaultTheme);
//    }
//
//    private void setupAppStateTracking() {
//        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//
//        AppStateTracker.init(this, Bus.getInstance());
//
//        if (activityManager.getAppTasks().size() > 0) {
//            AppStateTracker.getInstance().trackTask(activityManager.getAppTasks().get(0).getTaskInfo());
//        }
//    }
//}
