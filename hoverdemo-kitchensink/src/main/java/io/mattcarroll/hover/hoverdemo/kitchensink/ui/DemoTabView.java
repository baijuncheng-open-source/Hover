/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.ui;

import io.mattcarroll.hover.AnnotationDef.ColorInt;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.logging.Logger;

/**
 * Visual representation of a top-level tab in a Hover menu.
 */
public class DemoTabView extends Component implements Component.EstimateSizeListener {
    private static final String TAG = "DemoTabView";

    private int mBackgroundColor;
    private Integer mForegroundColor;

    private PixelMapElement mCircleDrawable;
    private PixelMapElement mIconDrawable;
    private int mIconInsetLeft, mIconInsetTop, mIconInsetRight, mIconInsetBottom;
    private Paint mPaint;

    public DemoTabView(Context context, PixelMapElement backgroundDrawable, PixelMapElement iconDrawable) {
        super(context);
        mCircleDrawable = backgroundDrawable;
        mIconDrawable = iconDrawable;
        init();
    }

    private void init() {
        ComponentContainer.LayoutConfig layoutConfig = getLayoutConfig();
        layoutConfig.width = 161;
        layoutConfig.height = 161;
        setLayoutConfig(layoutConfig);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        int density = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().densityDpi;
        int insetsDp = 10 * density;
        mIconInsetLeft = mIconInsetTop = mIconInsetRight = mIconInsetBottom = insetsDp;
        setEstimateSizeListener(this::onEstimateSize);
        addDrawTask(mDrawTask);
    }

    public void setTabBackgroundColor(@ColorInt int backgroundColor) {
        mBackgroundColor = backgroundColor;

        //need to do
        //mCircleDrawable.setColorFilter(mBackgroundColor, PorterDuff.Mode.SRC_ATOP);
    }

    public void setTabForegroundColor(@ColorInt Integer foregroundColor) {
        mForegroundColor = foregroundColor;
        if (null != mForegroundColor) {
            //mIconDrawable.setColorFilter(mForegroundColor, PorterDuff.Mode.SRC_ATOP);
        } else {
            //mIconDrawable.setColorFilter(null);
        }
    }

    public void setIcon(@Nullable PixelMapElement icon) {
        mIconDrawable = icon;
        if (null != mForegroundColor && null != mIconDrawable) {
            //mIconDrawable.setColorFilter(mForegroundColor, PorterDuff.Mode.SRC_ATOP);
        }
        updateIconBounds();

        invalidate();
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        int widthSpecSize = MeasureSpec.getSize(i);
        int heightSpecSize = MeasureSpec.getSize(i1);

        Logger.getLogger(TAG).severe("onEstimateSize x widthSpecSize=" + widthSpecSize);
        onSizeChanged(widthSpecSize, heightSpecSize);
        return false;
    }

    protected void onSizeChanged(int w, int h) {

        // Make circle as large as View minus padding.
        mCircleDrawable.setBounds(getPaddingLeft(), getPaddingTop(), w - getPaddingRight(), h - getPaddingBottom());

        // Re-size the icon as necessary.
        updateIconBounds();

        invalidate();
    }

    private void updateIconBounds() {
        if (null != mIconDrawable) {
            Rect bounds = new Rect(mCircleDrawable.getBounds());
            bounds.set(bounds.left + mIconInsetLeft, bounds.top + mIconInsetTop, bounds.right - mIconInsetRight, bounds.bottom - mIconInsetBottom);
            mIconDrawable.setBounds(bounds);
        }
    }

    private DrawTask mDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            canvas.drawPixelMapHolder(new PixelMapHolder(mCircleDrawable.getPixelMap()), 0, 0, mPaint);
            if (null != mIconDrawable) {
                canvas.drawPixelMapHolder(new PixelMapHolder(mIconDrawable.getPixelMap()), 33, 33, mPaint);
            }
        }
    };
}
