package io.mattcarroll.hover.hoverdemo.kitchensink;

import io.mattcarroll.hover.hoverdemo.kitchensink.appstate.AppStateTracker;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        AppStateTracker.init(this, Bus.getInstance());
    }
}
