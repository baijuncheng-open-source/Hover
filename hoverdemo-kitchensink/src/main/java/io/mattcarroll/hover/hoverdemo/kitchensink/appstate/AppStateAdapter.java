/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.appstate;

import java.util.ArrayList;
import java.util.List;

import io.mattcarroll.hover.AnnotationDef.ColorInt;
import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.hoverdemo.kitchensink.ResourceTable;
import io.mattcarroll.hover.hoverdemo.kitchensink.theming.HoverTheme;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * RecyclerView Adapter to display App Activitys and Services.
 */
public class AppStateAdapter extends RecycleItemProvider {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ACTIVITY = 1;
    private static final int VIEW_TYPE_SERVICE = 2;

    private HoverTheme mTheme;
    private Context mContext;
    private List<AppStateTracker.ActivityState> mActivityStates = new ArrayList<>();
    private List<AppStateTracker.ServiceState> mServiceStates = new ArrayList<>();

    public AppStateAdapter(@NonNull HoverTheme theme, Context context) {
        mTheme = theme;
        mContext = context;
    }

    public void setTheme(@NonNull HoverTheme theme) {
        mTheme = theme;
        //notifyDataChanged();
    }

    public void setActivityStates(List<AppStateTracker.ActivityState> activityStates) {
        mActivityStates.clear();
        mActivityStates.addAll(activityStates);
        notifyDataChanged();
    }

    public void setServiceStates(List<AppStateTracker.ServiceState> serviceStates) {
        mServiceStates.clear();
        mServiceStates.addAll(serviceStates);
        notifyDataChanged();
    }

    public int getItemViewType(int position) {
        if (0 == position) {
            return VIEW_TYPE_HEADER;
        } else if (position <= mActivityStates.size()) {
            return VIEW_TYPE_ACTIVITY;
        } else if (position == mActivityStates.size() + 1) {
            return VIEW_TYPE_HEADER;
        } else {
            return VIEW_TYPE_SERVICE;
        }
    }

    @Override
    public int getCount() {
        return 2 + mActivityStates.size() + mServiceStates.size();
    }

    public Object getItem(int position) {
        if (0 == position) {
            return "Activitys";
        } else if (position <= mActivityStates.size()) {
            // The Activity State list goes from deepest Activity to the highest, so process it in
            // reverse for presentation purposes.
            return mActivityStates.get(mActivityStates.size() - (position - 1) - 1);
        } else if (position == mActivityStates.size() + 1) {
            return "Services";
        } else {
            int index = position - mActivityStates.size() - 2;
            return mServiceStates.get(index);
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt = null;
        if (convertComponent == null) {
            int viewType = getItemViewType(position);
            switch (viewType) {
                case VIEW_TYPE_HEADER:
                    cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_view_app_state_header, componentContainer, false);
                    Text text1 = (Text) cpt.findComponentById(ResourceTable.Id_textview_header);
                    HeaderViewHolder holder1 = new HeaderViewHolder(text1);
                    holder1.setHeader((String) getItem(position), mTheme.getAccentColor());
                    break;
                case VIEW_TYPE_ACTIVITY:
                    cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_view_activity_state, componentContainer, false);
                    Text text2 = (Text) cpt.findComponentById(ResourceTable.Id_textview_activity_state);
                    ActivityStateViewHolder holder2 = new ActivityStateViewHolder(text2);
                    holder2.setActivityState((AppStateTracker.ActivityState) getItem(position));
                    break;
                case VIEW_TYPE_SERVICE:
                    cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_view_activity_state, componentContainer, false);
                    Text text3 = (Text) cpt.findComponentById(ResourceTable.Id_textview_activity_state);
                    ServiceStateViewHolder holder3 = new ServiceStateViewHolder(text3);
                    holder3.setServiceState((AppStateTracker.ServiceState) getItem(position));
                    break;
                default:
                    break;
            }
        } else {
            cpt = convertComponent;
        }
        return cpt;
    }

    private static class HeaderViewHolder {

        private Text mView;

        public HeaderViewHolder(Text itemView) {
            mView = itemView;
        }

        public void setHeader(@NonNull String header, @ColorInt int textColor) {
            mView.setText(header);
            mView.setTextColor(new Color(textColor));
        }
    }

    private static class ActivityStateViewHolder {

        private Text mView;

        public ActivityStateViewHolder(Text itemView) {
            mView = itemView;
        }

        public void setActivityState(@NonNull AppStateTracker.ActivityState activityState) {
            switch (activityState.getState()) {
                case CREATED:
                case STOPPED:
                    mView.setBackground(getShapeElement(mView.getContext(), ResourceTable.Graphic_rounded_rect_red));
                    break;
                case STARTED:
                case PAUSED:
                    mView.setBackground(getShapeElement(mView.getContext(), ResourceTable.Graphic_rounded_rect_yellow));
                    break;
                case RESUMED:
                    mView.setBackground(getShapeElement(mView.getContext(), ResourceTable.Graphic_rounded_rect_green));
                    break;
                case DESTROYED:
                    mView.setBackground(getShapeElement(mView.getContext(), ResourceTable.Graphic_rounded_rect_dark_gray));
                    break;
            }

            mView.setText(activityState.getActivityName());
        }
    }

    private static class ServiceStateViewHolder {

        private Text mView;

        public ServiceStateViewHolder(Text itemView) {
            mView = itemView;
        }

        public void setServiceState(@NonNull AppStateTracker.ServiceState serviceState) {
            ShapeElement shapeElement = getShapeElement(mView.getContext(), ResourceTable.Graphic_rounded_rect_green);
            mView.setBackground(shapeElement);
            mView.setText(serviceState.getServiceName());
        }
    }

    private static ShapeElement getShapeElement(Context context, int resId) {
        return new ShapeElement(context, resId);
    }
}
