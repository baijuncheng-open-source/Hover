/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.appstate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.greenrobot.event.EventBus;
import io.mattcarroll.hover.AnnotationDef.NonNull;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilityLifecycleCallbacks;
import ohos.aafwk.ability.AbilityMissionInfo;
import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.ability.RunningProcessInfo;
import ohos.app.IAbilityManager;
import ohos.os.ProcessManager;
import ohos.utils.PacMap;

/**
 * Tracks Activity and Service state and makes that information available to clients.
 */
public class AppStateTracker {

    private static final String TAG = "AppStateTracker";

    private static AppStateTracker sInstance;

    public static synchronized void init(@NonNull AbilityPackage application, @NonNull EventBus bus) {
        sInstance = new AppStateTracker(application, bus);
    }

    public static AppStateTracker getInstance() {
        if (null == sInstance) {
            throw new RuntimeException("Did you forget to call init()? You must initialize AppStateTracker before obtaining its instance.");
        }

        return sInstance;
    }

    private final AbilityPackage mApplication;
    private final EventBus mBus;
    private AbilityMissionInfo mTaskToTrack;
    private List<ActivityState> mActivityStates = new ArrayList<>();
    private List<ServiceState> mServiceStates = new ArrayList<>();
    private AbilityLifecycleCallbacks mActivityLifecycleCallback = new AbilityLifecycleCallbacks() {
        @Override
        public void onAbilityStart(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityCreated: " + ability.getLocalClassName());
            //activity.getTaskId()  need todo
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                mActivityStates.add(new ActivityState(ability.getLocalClassName(), ActivityState.State.CREATED));

                onActivityStackChanged();
            }

        }

        @Override
        public void onAbilityActive(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityResumed: " + ability.getLocalClassName());
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                int correspondingBackstackIndex = findFirstInBackStack(ability.getLocalClassName(), ActivityState.State.STARTED, ActivityState.State.PAUSED);
                if (correspondingBackstackIndex >= 0) {
                    // There is a corresponding Activity in the backstack, update it to RESUMED.
                    updateActivityState(correspondingBackstackIndex, ActivityState.State.RESUMED);
                }

                onActivityStackChanged();
            }
        }

        @Override
        public void onAbilityInactive(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityPaused: " + ability.getLocalClassName());
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                int correspondingBackstackIndex = findLastInBackStack(ability.getLocalClassName(), ActivityState.State.RESUMED);
                if (correspondingBackstackIndex >= 0) {
                    // There is a corresponding Activity in the backstack, update it to PAUSED.
                    updateActivityState(correspondingBackstackIndex, ActivityState.State.PAUSED);
                }

                onActivityStackChanged();
            }
        }

        @Override
        public void onAbilityForeground(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityStarted: " + ability.getLocalClassName());
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                // Note: When using the setting to forcibly kill Activitys, they go straight from destroyed to started.
                int correspondingBackstackIndex = findFirstInBackStack(ability.getLocalClassName(), ActivityState.State.CREATED, ActivityState.State.STOPPED, ActivityState.State.DESTROYED);
                if (correspondingBackstackIndex >= 0) {
                    // There is a corresponding Activity in the backstack, update it to STARTED.
                    updateActivityState(correspondingBackstackIndex, ActivityState.State.STARTED);
                }

                onActivityStackChanged();
            }
        }

        @Override
        public void onAbilityBackground(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityStopped: " + ability.getLocalClassName());
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                int correspondingBackstackIndex = findLastInBackStack(ability.getLocalClassName(), ActivityState.State.PAUSED, ActivityState.State.STARTED);
                if (correspondingBackstackIndex >= 0) {
                    // There is a corresponding Activity in the backstack, update it to STOPPED.
                    updateActivityState(correspondingBackstackIndex, ActivityState.State.STOPPED);
                }

                onActivityStackChanged();
            }

        }

        @Override
        public void onAbilityStop(Ability ability) {
            Logger.getLogger(TAG).severe("onActivityDestroyed: " + ability.getLocalClassName());
            if (1 == mTaskToTrack.getAbilityMissionId()) {
                int correspondingBackstackIndex = findLastInBackStack(ability.getLocalClassName(), ActivityState.State.STOPPED);
                if (correspondingBackstackIndex >= 0) {
                    if (!ability.isTerminating()) {
                        // There is a corresponding Activity in the backstack, update it to DESTROYED.
                        updateActivityState(correspondingBackstackIndex, ActivityState.State.DESTROYED);
                    } else {
                        mActivityStates.remove(correspondingBackstackIndex);
                    }
                }

                onActivityStackChanged();
            }
        }

        @Override
        public void onAbilitySaveState(PacMap pacMap) {

        }
    };

    public AppStateTracker(@NonNull AbilityPackage application, @NonNull EventBus bus) {
        mApplication = application;
        mBus = bus;
    }

    public void trackTask(@NonNull AbilityMissionInfo taskToTrack) {
        mTaskToTrack = taskToTrack;
        Logger.getLogger(TAG).severe("Task ID: " + taskToTrack.getAbilityMissionId());
        Logger.getLogger(TAG).severe("Original activity: " + taskToTrack.getAbilityBaseBundleName());

        if (null != mTaskToTrack.getAbilityBaseBundleName()) {
            mActivityStates.add(new ActivityState(mTaskToTrack.getAbilityBaseBundleName().getAbilityName(), ActivityState.State.CREATED));
        }

        mApplication.registerCallbacks(mActivityLifecycleCallback, null);
    }

    public List<ActivityState> getActivityStates() {
        return mActivityStates;
    }

    public List<ServiceState> getServiceStates() {
        IAbilityManager activityManager = mApplication.getAbilityManager();
        List<ohos.aafwk.ability.RunningProcessInfo> runningServices = activityManager.getAllRunningProcesses();
        List<ServiceState> serviceStates = new ArrayList<>(runningServices.size());
        for (RunningProcessInfo info : runningServices) {
            if (info.getPid() == ProcessManager.getPid()) {
                serviceStates.add(new ServiceState(info.getProcessName()));
            }
        }
        return serviceStates;
    }

    private int findFirstInBackStack(@NonNull String activityName, @NonNull ActivityState.State... statesToFind) {
        ActivityState activityState;
        for (int i = mActivityStates.size() - 1; i >= 0; --i) {
            activityState = mActivityStates.get(i);
            if (activityState.getActivityName().equals(activityName)) {
                for (int j = 0; j < statesToFind.length; ++j) {
                    if (statesToFind[j] == activityState.getState()) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    private int findLastInBackStack(@NonNull String activityName, @NonNull ActivityState.State... statesToFind) {
        ActivityState activityState;
        for (int i = 0; i < mActivityStates.size(); ++i) {
            activityState = mActivityStates.get(i);
            if (activityState.getActivityName().equals(activityName)) {
                for (int j = 0; j < statesToFind.length; ++j) {
                    if (statesToFind[j] == activityState.getState()) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    private void updateActivityState(int index, ActivityState.State newState) {
        ActivityState activityState = mActivityStates.remove(index);
        mActivityStates.add(index, new ActivityState(activityState.getActivityName(), newState));
    }

    private void onActivityStackChanged() {
        logStack();
        mBus.postSticky(new ActivityStackChangeEvent(mActivityStates));
    }

    private void logStack() {
        Logger.getLogger(TAG).severe("Stack:");
        ActivityState activityState;
        for (int i = mActivityStates.size() - 1; i >= 0; --i) {
            activityState = mActivityStates.get(i);
            Logger.getLogger(TAG).severe(" " + i + " - " + activityState.getActivityName() + ": " + activityState.getState());
        }
        Logger.getLogger(TAG).severe(" ");
    }

    public static class ActivityState {

        public enum State {
            CREATED,
            STARTED,
            RESUMED,
            PAUSED,
            STOPPED,
            DESTROYED
        }

        private String mActivityName;
        private State mState;

        private ActivityState(@NonNull String name, @NonNull State state) {
            mActivityName = name;
            mState = state;
        }

        public String getActivityName() {
            return mActivityName;
        }

        public State getState() {
            return mState;
        }

    }

    public static class ServiceState {

        private String mServiceName;

        public ServiceState(@NonNull String serviceName) {
            mServiceName = serviceName;
        }

        public String getServiceName() {
            return mServiceName;
        }

    }

    public static class ActivityStackChangeEvent {

        private List<ActivityState> mBackstack;

        public ActivityStackChangeEvent(List<ActivityState> backstack) {
            mBackstack = backstack;
        }

        public List<ActivityState> getActivityStack() {
            return mBackstack;
        }
    }

}
