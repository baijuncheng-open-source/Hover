/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.introduction;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.time.Clock;
import java.time.ZoneId;
import java.util.logging.Logger;

/**
 * Moves and scales a View as if its hovering.
 */
public class HoverMotion {

    private static final String TAG = "HoverMotion";

    private static final int RENDER_CYCLE_IN_MILLIS = 16; // 60 FPS.

    private Component mView;
    private BrownianMotionGenerator mBrownianMotionGenerator = new BrownianMotionGenerator();
    private GrowAndShrinkGenerator mGrowAndShrinkGenerator = new GrowAndShrinkGenerator(0.05f);
    private boolean mIsRunning;
    private long mTimeOfLastUpdate;
    private int mDtInMillis;
    private EventHandler mUIHandle = new EventHandler(EventRunner.getMainEventRunner());
    private Runnable mStateUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (mIsRunning) {
                // Calculate the time that's passed since the last update.
                mDtInMillis = (int) (Clock.system(ZoneId.systemDefault()).millis() - mTimeOfLastUpdate);

                // Update visual state.
                updatePosition();
                updateGrowth();

                // Update time tracking.
                mTimeOfLastUpdate = Clock.system(ZoneId.systemDefault()).millis();

                // Schedule next loop.
                mUIHandle.postTask(this, RENDER_CYCLE_IN_MILLIS);
            }
        }
    };

    public void start(@NonNull Component view) {
        Logger.getLogger(TAG).severe("start()");
        mView = view;
        mIsRunning = true;
        mTimeOfLastUpdate = Clock.system(ZoneId.systemDefault()).millis();
        mUIHandle.postTask(mStateUpdateRunnable);
    }

    public void stop() {
        Logger.getLogger(TAG).severe("stop()");
        mIsRunning = false;
    }

    private void updatePosition() {
        // Calculate a new position and move the View.
        Point mPositionOffset = mBrownianMotionGenerator.applyMotion(mDtInMillis);
        mView.setTranslationX(mPositionOffset.getPointX());
        mView.setTranslationY(mPositionOffset.getPointY());
    }

    private void updateGrowth() {
        // Calculate and apply scaling.
        float scale = mGrowAndShrinkGenerator.applyGrowth(mDtInMillis);
        mView.setScaleX(scale);
        mView.setScaleY(scale);

        // Set elevation based on scale (the bigger, the higher).
        int baseElevation = 50;
        //mView.setElevation(baseElevation * scale);
    }

    public static class BrownianMotionGenerator {

        private static final float FRICTION = 0.8f;

        private int mMaxDisplacementInPixels = 200;
        private Point mPosition = new Point(0, 0);
        private Point mVelocity = new Point(0, 0);

        public Point applyMotion(int dtInMillis) {
            float xConstraintAdditive = mPosition.getPointX() / mMaxDisplacementInPixels;
            float yConstraintAdditive = mPosition.getPointY() / mMaxDisplacementInPixels;

            // Randomly adjust velocity.
//            int randomNum = 100;
//            try {
//                SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
//                randomNum = random.nextInt();
//            } catch (NoSuchAlgorithmException e) {
//
//            }
            float velocityXAdjustment = (float) (Math.random() * 1.0 - 0.5) - xConstraintAdditive;
            float velocityYAdjustment = (float) (Math.random() * 1.0 - 0.5) - yConstraintAdditive;
            mVelocity.translate(velocityXAdjustment, velocityYAdjustment);

            // Apply velocity to position.
            mPosition.translate(mVelocity.getPointX(), mVelocity.getPointY());

            // Apply friction to velocity.
            mVelocity.modify(mVelocity.getPointX() * FRICTION, mVelocity.getPointY() * FRICTION);

            return mPosition;
        }

    }

    public static class GrowAndShrinkGenerator {

        private static final int GROWTH_CYCLE_IN_MILLIS = 5000;

        private int mLastTimeInMillis;
        private float mGrowthFactor;

        public GrowAndShrinkGenerator(float growthFactor) {
            mGrowthFactor = growthFactor;
        }

        public float applyGrowth(int dtInMillis) {
            mLastTimeInMillis += dtInMillis;
            return 1.0f + (float) (Math.sin(Math.PI * ((float) mLastTimeInMillis / GROWTH_CYCLE_IN_MILLIS)) * mGrowthFactor);
        }

    }
}
