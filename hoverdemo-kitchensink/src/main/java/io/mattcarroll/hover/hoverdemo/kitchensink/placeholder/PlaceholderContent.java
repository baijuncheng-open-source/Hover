/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink.placeholder;


import de.greenrobot.event.EventBus;
import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.Content;
import io.mattcarroll.hover.hoverdemo.kitchensink.ResourceTable;
import io.mattcarroll.hover.hoverdemo.kitchensink.theming.HoverTheme;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.logging.Logger;

/**
 * Use this class to try adding your own content to the Hover menu.
 */
public class PlaceholderContent extends StackLayout implements Content, Component.BindStateChangedListener {

    private final EventBus mBus;
    private Text mTitleTextView;
    private static final String TAG = "PlaceholderContent";

    public PlaceholderContent(@NonNull Context context, @NonNull EventBus bus) {
        super(context);
        mBus = bus;
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_placeholder_content, this, true);
        mTitleTextView = (Text) findComponentById(ResourceTable.Id_textview_title);
        Logger.getLogger(TAG).severe("init  mTitleTextView=" + mTitleTextView);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        mBus.registerSticky(this);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mBus.unregister(this);
    }

    @NonNull
    @Override
    public Component getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {

    }

    @Override
    public void onHidden() {

    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mTitleTextView.setTextColor(new Color(newTheme.getAccentColor()));
    }
}
