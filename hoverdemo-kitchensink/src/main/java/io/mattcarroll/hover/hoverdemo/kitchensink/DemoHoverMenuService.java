/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.hoverdemo.kitchensink;

import java.util.logging.Logger;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.HoverMenu;
import io.mattcarroll.hover.HoverView;
import io.mattcarroll.hover.window.HoverMenuService;
import io.mattcarroll.hover.hoverdemo.kitchensink.theming.HoverTheme;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;

/**
 * Demo {@link HoverMenuService}.
 */
public class DemoHoverMenuService extends HoverMenuService {

    private static final String TAG = "DemoHoverMenuService";

    public static void showFloatingMenu(Context context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("io.mattcarroll.hover.hoverdemo.kitchensink")
                .withAbilityName("io.mattcarroll.hover.hoverdemo.kitchensink.DemoHoverMenuService")
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, 0);
    }

    private DemoHoverMenu mDemoHoverMenu;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Bus.getInstance().register(this);
    }


    @Override
    protected void onStop() {
        Logger.getLogger(TAG).severe("onStop");
        Bus.getInstance().unregister(this);
        super.onStop();
    }


    @Override
    protected Context getContextForHoverMenu() {
        return this;
    }

    @Override
    protected void onHoverMenuLaunched(@NonNull Intent intent, @NonNull HoverView hoverView) {
        hoverView.setMenu(createHoverMenu());
        hoverView.collapse();
    }

    private HoverMenu createHoverMenu() {
        mDemoHoverMenu = new DemoHoverMenuFactory().createDemoMenuFromCode(getContextForHoverMenu(), Bus.getInstance());
//            mDemoHoverMenuAdapter = new DemoHoverMenuFactory().createDemoMenuFromFile(getContextForHoverMenu());
        return mDemoHoverMenu;
    }

    public void onEventMainThread(@NonNull HoverTheme newTheme) {
        mDemoHoverMenu.setTheme(newTheme);
    }

}
