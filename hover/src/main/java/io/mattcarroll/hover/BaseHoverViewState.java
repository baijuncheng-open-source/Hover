/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.service.Window;

import java.util.logging.Logger;

/**
 * {@link HoverViewState} that includes behavior common to all implementations.
 */
abstract class BaseHoverViewState implements HoverViewState {

    private HoverView mHoverView;
    private static final String TAG = "BaseHoverViewState";
    private Window mWindow;

    @Override
    public void takeControl(@NonNull HoverView hoverView) {
        mHoverView = hoverView;
    }

    // Only call this if using HoverMenuView directly in a window.
    @Override
    public void addToWindow() {
        //ComponentContainer.LayoutConfig.MATCH_PARENT
        if (!mHoverView.mIsAddedToWindow) {
            mWindow = mHoverView.mWindowViewController.addView(mHoverView.getContext(),
                    ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT,
                    false,
                    mHoverView
            );
            mHoverView.mWindow = mWindow;

            Logger.getLogger(TAG).severe("addToWindow  window =" + mWindow);
            mHoverView.mIsAddedToWindow = true;

            if (mHoverView.mIsTouchableInWindow) {
                mHoverView.makeTouchableInWindow();
            } else {
                mHoverView.makeUntouchableInWindow();
            }
        }
    }

    // Only call this if using HoverMenuView directly in a window.
    @Override
    public void removeFromWindow() {
        Logger.getLogger(TAG).severe("removeView  mHoverView.mIsAddedToWindow=" + mHoverView.mIsAddedToWindow + " mHoverView.mWindow=" + mHoverView.mWindow);
        if (mHoverView.mIsAddedToWindow && mHoverView.mWindow != null) {
            mHoverView.mWindowViewController.removeView(mHoverView, mHoverView.mWindow);
            mHoverView.mIsAddedToWindow = false;
        }
    }

    @Override
    public void makeTouchableInWindow() {
        mHoverView.mIsTouchableInWindow = true;
        Logger.getLogger(TAG).severe("makeTouchableInWindow  mHoverView.mIsAddedToWindow=" + mHoverView.mIsAddedToWindow);
        if (mHoverView.mIsAddedToWindow) {
            mHoverView.mWindowViewController.makeTouchable(mHoverView,mHoverView.mWindow);
        }
    }

    @Override
    public void makeUntouchableInWindow() {
        mHoverView.mIsTouchableInWindow = false;
        Logger.getLogger(TAG).severe("makeUntouchableInWindow  mHoverView.mIsAddedToWindow=" + mHoverView.mIsAddedToWindow);
        if (mHoverView.mIsAddedToWindow) {
            mHoverView.mWindowViewController.makeUntouchable(mHoverView,mHoverView.mWindow);
        }
    }
}
