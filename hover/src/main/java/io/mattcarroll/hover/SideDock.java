/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;


import io.mattcarroll.hover.AnnotationDef.IntDef;
import io.mattcarroll.hover.AnnotationDef.NonNull;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Point;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.logging.Logger;

/**
 * {@link Dock} that always positions itself either on the left or right side of its container. A
 * {@code SideDock} insets itself slightly from the edge of its container based on a proportion of
 * a tab's size.
 */
public class SideDock extends Dock {

    private static final String TAG = "SideDock";

    private ComponentContainer mContainerView;
    private int mTabSize;
    private SidePosition mSidePosition;

    SideDock(@NonNull ComponentContainer containerView, int tabSize, @NonNull SidePosition sidePosition) {
        mContainerView = containerView;
        mTabSize = tabSize;
        mSidePosition = sidePosition;
    }

    @NonNull
    @Override
    public Point position() {
        Point screenSize = new Point(mContainerView.getEstimatedWidth(), mContainerView.getEstimatedHeight());
        return mSidePosition.calculateDockPosition(screenSize, mTabSize);
    }

    @NonNull
    public SidePosition sidePosition() {
        return mSidePosition;
    }

    @Override
    public String toString() {
        return mSidePosition.toString();
    }

    public static class SidePosition {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({LEFT, RIGHT})
        public @interface Side {
        }

        public static final int LEFT = 0;
        public static final int RIGHT = 1;

        @Side
        private int mSide;
        private float mVerticalDockPositionPercentage;

        public SidePosition(@Side int side, float verticalDockPositionPercentage) {
            mSide = side;
            mVerticalDockPositionPercentage = verticalDockPositionPercentage;
        }

        public Point calculateDockPosition(@NonNull Point screenSize, int tabSize) {
            Logger.getLogger(TAG).severe("Calculating dock position. Screen size: " + screenSize + ", tab size: " + tabSize);
            int x = LEFT == mSide
                    ? ((int) (tabSize * 0.25))
                    : (int) screenSize.getPointX() - ((int) (tabSize * 0.25));

            int y = (int) (screenSize.getPointY() * mVerticalDockPositionPercentage);

            return new Point(x, y);
        }

        public float getVerticalDockPositionPercentage() {
            return mVerticalDockPositionPercentage;
        }

        @Side
        public int getSide() {
            return mSide;
        }

        @Override
        public String toString() {
            String side = LEFT == mSide ? "Left" : "Right";
            int percent = (int) Math.floor(mVerticalDockPositionPercentage * 100);
            return String.format("%s side at %d%%", side, percent);
        }
    }
}
