/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.view;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.Dragger;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.multimodalinput.event.TouchEvent;

import java.util.logging.Logger;

/**
 * {@link Dragger} implementation that works within a {@link ComponentContainer}.
 */
public class InViewDragger implements Dragger {

    private static final String TAG = "InViewDragger";

    private ComponentContainer mContainer;
    private int mTouchAreaDiameter;
    private int mTapTouchSlop;
    private boolean mIsActivated;
    private boolean mIsDragging;
    private boolean mIsDebugMode = false;
    private Component mDragView;
    private DragListener mDragListener;
    private Point mOriginalViewPosition = new Point();
    private Point mCurrentViewPosition = new Point();
    private Point mOriginalTouchPosition = new Point();

    private final Component.TouchEventListener mDragTouchListener = new Component.TouchEventListener() {
        @Override
        public boolean onTouchEvent(Component component, TouchEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    Logger.getLogger(TAG).severe("ACTION_DOWN ");
                    mIsDragging = false;

                    mOriginalViewPosition = getDragViewCenterPosition();
                    mCurrentViewPosition = new Point(mOriginalViewPosition.getPointX(), mOriginalViewPosition.getPointY());
                    mOriginalTouchPosition.modify(motionEvent.getPointerPosition(0).getX(), motionEvent.getPointerPosition(0).getY());

                    mDragListener.onPress(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());

                    return true;
                case TouchEvent.POINT_MOVE:
                    Logger.getLogger(TAG).severe("ACTION_MOVE. motionX: " + motionEvent.getPointerPosition(0).getX() + ", motionY: " + motionEvent.getPointerPosition(0).getY());
                    float dragDeltaX = motionEvent.getPointerPosition(0).getX() - mOriginalTouchPosition.getPointX();
                    float dragDeltaY = motionEvent.getPointerPosition(0).getY() - mOriginalTouchPosition.getPointY();
                    mCurrentViewPosition = new Point(
                            mOriginalViewPosition.getPointX() + dragDeltaX,
                            mOriginalViewPosition.getPointY() + dragDeltaY
                    );

                    if (mIsDragging || !isTouchWithinSlopOfOriginalTouch(dragDeltaX, dragDeltaY)) {
                        if (!mIsDragging) {
                            // Dragging just started
                            Logger.getLogger(TAG).severe("MOVE Start Drag.");
                            mIsDragging = true;
                            mDragListener.onDragStart(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                        } else {
                            //moveDragViewTo(mCurrentViewPosition);
                            mDragListener.onDragTo(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                        }
                    }

                    return true;
                case TouchEvent.PRIMARY_POINT_UP:
                    if (!mIsDragging) {
                        Logger.getLogger(TAG).severe("ACTION_UP: Tap.");
                        mDragListener.onTap();
                    } else {
                        Logger.getLogger(TAG).severe("ACTION_UP: Released from dragging.");
                        mDragListener.onReleasedAt(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                    }

                    return true;
                default:
                    return false;
            }
        }
    };

    public InViewDragger(@NonNull ComponentContainer container, int touchAreaDiameter, int touchSlop) {
        mContainer = container;
        mTouchAreaDiameter = touchAreaDiameter;
        mTapTouchSlop = touchSlop;
    }

    @Override
    public void enableDebugMode(boolean isDebugMode) {
        mIsDebugMode = isDebugMode;
        updateTouchControlViewAppearance();
    }

    @Override
    public void activate(@NonNull DragListener dragListener, @NonNull Point dragStartCenterPosition) {
        if (!mIsActivated) {
            Logger.getLogger(TAG).severe("Activating.");
            mIsActivated = true;
            mDragListener = dragListener;
            createTouchControlView(dragStartCenterPosition);
        }
    }

    @Override
    public void deactivate() {
        if (mIsActivated) {
            Logger.getLogger(TAG).severe("Deactivating.");
            mIsActivated = false;
            destroyTouchControlView();
        }
    }

    private void createTouchControlView(@NonNull Point dragStartCenterPosition) {
        mDragView = new Component(mContainer.getContext());
        mDragView.setId(001);
        mDragView.setLayoutConfig(new ComponentContainer.LayoutConfig(mTouchAreaDiameter,mTouchAreaDiameter));
        mDragView.setTouchEventListener(mDragTouchListener);
        mContainer.addComponent(mDragView);

        moveDragViewTo(new Point(dragStartCenterPosition.getPointX(), dragStartCenterPosition.getPointY()));
        updateTouchControlViewAppearance();
    }

    private void destroyTouchControlView() {
        mContainer.removeComponent(mDragView);
        mDragView.setTouchEventListener(null);

        mDragView = null;
    }

    private void updateTouchControlViewAppearance() {
        if (null != mDragView) {
            ShapeElement element = new ShapeElement();
            if (mIsDebugMode) {
                Logger.getLogger(TAG).severe("Making mDragView red: " + mDragView.hashCode());
                element.setRgbColor(new RgbColor(Color.getIntColor("#44FF0000")));
            } else {
                element.setRgbColor(new RgbColor(Color.getIntColor("#00000000")));
            }
            mDragView.setBackground(element);
        }
    }

    private boolean isTouchWithinSlopOfOriginalTouch(float dx, float dy) {
        double distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        return distance < mTapTouchSlop;
    }

    private Point getDragViewCenterPosition() {
        return convertCornerToCenter(new Point(
                mDragView.getTranslationX(),
                mDragView.getTranslationY()
        ));
    }

    private void moveDragViewTo(Point centerPosition) {
        Logger.getLogger(TAG).severe("Moving drag view (" + mDragView.hashCode() + ") to: " + centerPosition);
        Point cornerPosition = convertCenterToCorner(centerPosition);
        mDragView.setTranslationX(cornerPosition.getPointX());
        mDragView.setTranslationY(cornerPosition.getPointY());
    }

    private Point convertCornerToCenter(@NonNull Point cornerPosition) {
        return new Point(
                cornerPosition.getPointX() + (mTouchAreaDiameter / 2),
                cornerPosition.getPointY() + (mTouchAreaDiameter / 2)
        );
    }

    private Point convertCenterToCorner(@NonNull Point centerPosition) {
        return new Point(
                centerPosition.getPointX() - (mTouchAreaDiameter / 2),
                centerPosition.getPointY() - (mTouchAreaDiameter / 2)
        );
    }
}
