/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Logger;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import io.mattcarroll.hover.view.InViewDragger;
import io.mattcarroll.hover.window.InWindowDragger;
import io.mattcarroll.hover.window.WindowViewController;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.window.service.Window;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;

import static io.mattcarroll.hover.SideDock.SidePosition.LEFT;

/**
 * {@code HoverMenuView} is a floating menu implementation. This implementation displays tabs along
 * the top of its display, from right to left. Below the tabs, filling the remainder of the display
 * is a content region that displays the content for a selected tab.  The content region includes
 * a visual indicator showing which tab is currently selected.  Each tab's content includes a title
 * and a visual area.  The visual area can display any {@link Content}.
 */
public class HoverView extends DependentLayout {

    private static final String TAG = "HoverView";

    private static final String PREFS_FILE = "hover";
    private static final String SAVED_STATE_DOCK_POSITION = "_dock_position";
    private static final String SAVED_STATE_DOCKS_SIDE = "_dock_side";
    private static final String SAVED_STATE_SELECTED_SECTION = "_selected_section";

    @NonNull
    public static HoverView createForWindow(@NonNull Context context,
                                            @NonNull WindowViewController windowViewController) {
        return createForWindow(context, windowViewController, null);
    }

    @NonNull
    public static HoverView createForWindow(@NonNull Context context,
                                            @NonNull WindowViewController windowViewController,
                                            @Nullable SideDock.SidePosition initialDockPosition) {
        Dragger dragger = createWindowDragger(context, windowViewController);

        Logger.getLogger(TAG).severe("createWindowDragger  dragger=" + dragger);
        return new HoverView(context, dragger, windowViewController, initialDockPosition);
    }

    @NonNull
    private static Dragger createWindowDragger(@NonNull Context context,
                                               @NonNull WindowViewController windowViewController) {
        int touchDiameter = 225;
        int slop = 10;
        return new InWindowDragger(
                context,
                windowViewController,
                touchDiameter,
                slop
        );
    }

    @NonNull
    public static HoverView createForView(@NonNull Context context) {
        return new HoverView(context, null);
    }

    final HoverViewState mClosed = new HoverViewStateClosed();
    final HoverViewState mCollapsed = new HoverViewStateCollapsed();
    final HoverViewState mExpanded = new HoverViewStateExpanded();
    final WindowViewController mWindowViewController;
    final Dragger mDragger;
    public final Screen mScreen;
    public HoverViewState mState;
    HoverMenu mMenu;
    public HoverMenu.SectionId mSelectedSectionId;
    SideDock mCollapsedDock;
    boolean mIsAddedToWindow;
    boolean mIsTouchableInWindow;
    boolean mIsDebugMode = false;
    Window mWindow;
    int mTabSize;
    OnExitListener mOnExitListener;
    final Set<Listener> mListeners = new CopyOnWriteArraySet<>();

    // Public for use with XML inflation. Clients should use static methods for construction.
    public HoverView(@NonNull Context context, @Nullable AttrSet attrs) {
        super(context, attrs);
        mDragger = createInViewDragger(context);
        mScreen = new Screen(this);
        mWindowViewController = null;

        init();

        if (null != attrs) {
            applyAttributes(attrs);
        }
    }

    @NonNull
    private Dragger createInViewDragger(@NonNull Context context) {
        int touchDiameter = 225;
        int slop = 10;
        return new InViewDragger(
                this,
                touchDiameter,
                slop
        );
    }

    private HoverView(@NonNull Context context,
                      @NonNull Dragger dragger,
                      @Nullable WindowViewController windowViewController,
                      @Nullable SideDock.SidePosition initialDockPosition) {
        super(context);
        mDragger = dragger;
        mScreen = new Screen(this);
        mWindowViewController = windowViewController;

        init();

        if (null != initialDockPosition) {
            mCollapsedDock = new SideDock(
                    this,
                    mTabSize,
                    initialDockPosition
            );
        }
    }

    private void applyAttributes(@NonNull AttrSet attrs) {
        int tabSize = 216;
        @SideDock.SidePosition.Side
        int dockSide = LEFT;
        if (attrs.getAttr("dockSide").isPresent()) {
            dockSide = attrs.getAttr("dockSide").get().getIntegerValue();
        }
        float dockPosition = 0.5f;
        if (attrs.getAttr("dockPosition").isPresent()) {
            dockPosition = attrs.getAttr("dockPosition").get().getFloatValue();
        }
        SideDock.SidePosition sidePosition = new SideDock.SidePosition(dockSide, dockPosition);
        mCollapsedDock = new SideDock(
                this,
                tabSize,
                sidePosition
        );
    }

    private void init() {
        mTabSize = 216;
        restoreVisualState();
        setTouchFocusable(true); // For handling hardware back button presses.
        setState(new HoverViewStateClosed());

        setTouchFocusable(true);
        requestFocus();
        setKeyEventListener(new KeyEventListener() {
            @Override
            public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
                if(mState.respondsToBackButton() && keyEvent.getKeyCode() == KeyEvent.KEY_BACK){
                    onBackPressed();
                    return true;
                }
                return false;
            }
        });
    }

    public void saveVisualState() {
        if (null == mMenu) {
            // Nothing to save.
            return;
        }
    }

    void restoreVisualState() {
        if (null == mMenu) {
            Logger.getLogger(TAG).severe("Tried to restore visual state but no menu set.");
            return;
        }
    }

    // TODO: when to call this?
    public void release() {
        Logger.getLogger(TAG).severe("Released.");
        mDragger.deactivate();
        // TODO: should we also release the screen?
    }

    public void enableDebugMode(boolean debugMode) {
        mIsDebugMode = debugMode;

        mDragger.enableDebugMode(debugMode);
        mScreen.enableDrugMode(debugMode);
    }

    void setState(@NonNull HoverViewState state) {
        Logger.getLogger(TAG).severe("setState state =" + state);
        mState = state;
        mState.takeControl(this);
    }

    public void onBackPressed() {
        mState.onBackPressed();
    }

    public void setMenu(@Nullable HoverMenu menu) {
        mState.setMenu(menu);
    }

    public void expand() {
        mState.expand();
    }

    public void collapse() {
        mState.collapse();
    }

    public void close() {
        mState.close();
    }

    public void setOnExitListener(@Nullable OnExitListener listener) {
        mOnExitListener = listener;
    }

    public void addOnExpandAndCollapseListener(@NonNull Listener listener) {
        mListeners.add(listener);
    }

    public void removeOnExpandAndCollapseListener(@NonNull Listener listener) {
        mListeners.remove(listener);
    }

    void notifyListenersExpanding() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is expanding.");
        for (Listener listener : mListeners) {
            listener.onExpanding();
        }
    }

    void notifyListenersExpanded() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is now expanded.");
        for (Listener listener : mListeners) {
            listener.onExpanded();
        }
    }

    void notifyListenersCollapsing() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is collapsing.");
        for (Listener listener : mListeners) {
            listener.onCollapsing();
        }
    }

    void notifyListenersCollapsed() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is now collapsed.");
        for (Listener listener : mListeners) {
            listener.onCollapsed();
        }
    }

    void notifyListenersClosing() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is closing.");
        for (Listener listener : mListeners) {
            listener.onClosing();
        }
    }

    void notifyListenersClosed() {
        Logger.getLogger(TAG).severe("Notifying listeners that Hover is closed.");
        for (Listener listener : mListeners) {
            listener.onClosed();
        }
    }

    // Only call this if using HoverMenuView directly in a window.
    public void addToWindow() {
        mState.addToWindow();
    }

    // Only call this if using HoverMenuView directly in a window.
    public void removeFromWindow() {
        mState.removeFromWindow();
    }

    void makeTouchableInWindow() {
        mState.makeTouchableInWindow();
    }

    void makeUntouchableInWindow() {
        mState.makeUntouchableInWindow();
    }


    /**
     * Listener invoked when the corresponding transitions occur within a given {@link HoverView}.
     */
    public interface Listener {

        void onExpanding();

        void onExpanded();

        void onCollapsing();

        void onCollapsed();

        void onClosing();

        void onClosed();

    }
}
