/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import io.mattcarroll.hover.window.InWindowDragger;
import ohos.agp.components.Component;
import ohos.agp.utils.Point;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.logging.Logger;

import static ohos.agp.components.Component.HIDE;
import static ohos.agp.components.Component.INVISIBLE;
import static ohos.agp.components.Component.VISIBLE;


/**
 * {@link HoverViewState} that operates the {@link HoverView} when it is collapsed. Collapsed means
 * that the only thing visible is the selected {@link FloatingTab}.  This tab docks itself against
 * the left or right sides of the screen.  The user can drag the tab around and drop it.
 * <p>
 * If the tab is tapped, the {@code HoverView} is transitioned to its expanded state.
 * <p>
 * If the tab is dropped on the exit region, the {@code HoverView} is transitioned to its closed state.
 */
class HoverViewStateCollapsed extends BaseHoverViewState {

    private static final String TAG = "HoverViewStateCollapsed";

    private HoverView mHoverView;
    private FloatingTab mFloatingTab;
    private HoverMenu.Section mSelectedSection;
    private int mSelectedSectionIndex = -1;
    private boolean mHasControl = false;
    private boolean mIsCollapsed = false;
    private boolean mIsDocked = false;
    private Dragger.DragListener mDragListener;
    private Listener mListener;
    private EventHandler mUIHandle = new EventHandler(EventRunner.getMainEventRunner());

    private final Component.LayoutRefreshedListener mOnLayoutChangeListener = new Component.LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            if (mHasControl && mIsDocked) {
                // We're docked. Adjust the tab position in case the screen was rotated. This should
                // only be a concern when displaying as a window overlay, but not when displaying
                // within a view hierarchy.
                moveToDock();
            }
        }
    };

    HoverViewStateCollapsed() {
    }

    @Override
    public void takeControl(@NonNull HoverView hoverView) {
        Logger.getLogger(TAG).severe("Taking control.");

        super.takeControl(hoverView);

        if (mHasControl) {
            Logger.getLogger(TAG).severe("Already has control.");
            return;
        }

        Logger.getLogger(TAG).severe("takeControl Instructing tab to dock itself. mHoverView=" + mHoverView);
        mHasControl = true;
        mHoverView = hoverView;
        mHoverView.mState = this;
        mHoverView.clearFocus(); // For handling hardware back button presses.
        mHoverView.mScreen.getContentDisplay().setVisibility(HIDE);
        mHoverView.makeUntouchableInWindow();

        Logger.getLogger(TAG).severe("Taking control with selected section: " + mHoverView.mSelectedSectionId);
        mSelectedSection = mHoverView.mMenu.getSection(mHoverView.mSelectedSectionId);
        mSelectedSection = null != mSelectedSection ? mSelectedSection : mHoverView.mMenu.getSection(0);
        mSelectedSectionIndex = mHoverView.mMenu.getSectionIndex(mSelectedSection);
        mFloatingTab = mHoverView.mScreen.getChainedTab(mHoverView.mSelectedSectionId);
        final boolean wasFloatingTabVisible;
        if (null == mFloatingTab) {
            wasFloatingTabVisible = false;
            mFloatingTab = mHoverView.mScreen.createChainedTab(mHoverView.mSelectedSectionId, mSelectedSection.getTabView(), false);
            Logger.getLogger(TAG).severe("Taking control mFloatingTab =" + mFloatingTab);
        } else {
            wasFloatingTabVisible = true;
        }
        mDragListener = new FloatingTabDragListener(this);
        mIsCollapsed = false; // We're collapsing, not yet collapsed.
        if (null != mListener) {
            mHoverView.notifyListenersCollapsing();
            mListener.onCollapsing();
        }
        initDockPosition();

        // post() animation to dock in case the container hasn't measured itself yet.
        if (!wasFloatingTabVisible) {
            mFloatingTab.setVisibility(INVISIBLE);
        }
        mUIHandle.postTask(new Runnable() {
            @Override
            public void run() {

                if (wasFloatingTabVisible) {
                    sendToDock(0, 0);
                } else {
                    mFloatingTab.setVisibility(VISIBLE);
                    moveToDock();
                    onDocked();
                }
            }
        });

        mFloatingTab.setLayoutRefreshedListener(mOnLayoutChangeListener);

        if (null != mHoverView.mMenu) {
            listenForMenuChanges();
        }
    }

    @Override
    public void expand() {
        if (mHoverView != null) {
            changeState(mHoverView.mExpanded);
        }
    }

    @Override
    public void collapse() {
        Logger.getLogger(TAG).severe("Instructed to collapse, but already collapsed.");
    }

    @Override
    public void close() {
        changeState(mHoverView.mClosed);
    }

    private void changeState(@NonNull HoverViewState nextState) {
        Logger.getLogger(TAG).severe("changeState Giving up control.");
        if (!mHasControl) {
            throw new RuntimeException("Cannot give control to another HoverMenuController when we don't have the HoverTab.");
        }

        mFloatingTab.setLayoutRefreshedListener(null);

        if (null != mHoverView.mMenu) {
            // mHoverView.mMenu.setUpdatedCallback(null);
        }

        mHasControl = false;
        mIsDocked = false;
        deactivateDragger();
        mDragListener = null;
        mFloatingTab = null;

        mHoverView.setState(nextState);

        mHoverView = null;
    }

    @Override
    public void setMenu(@Nullable final HoverMenu menu) {
        mHoverView.mMenu = menu;

        // If the menu is null or empty then we can't be collapsed, close the menu.
        if (null == menu || menu.getSectionCount() == 0) {
            close();
            return;
        }

        mHoverView.restoreVisualState();

        if (null == mHoverView.mSelectedSectionId || null == mHoverView.mMenu.getSection(mHoverView.mSelectedSectionId)) {
            mHoverView.mSelectedSectionId = mHoverView.mMenu.getSection(0).getId();
        }

        listenForMenuChanges();
    }

    private void listenForMenuChanges() {
    }

    @Override
    public boolean respondsToBackButton() {
        return false;
    }

    @Override
    public void onBackPressed() {
        closeMenu(new Runnable() {
            @Override
            public void run() {
                if (null != mHoverView.mOnExitListener) {
                    mHoverView.mOnExitListener.onExit();
                }
            }
        });
    }

    public void setListener(@Nullable Listener listener) {
        mListener = listener;
    }

    private void onPickedUpByUser() {
        mIsDocked = false;
        mHoverView.mScreen.getExitView().setVisibility(VISIBLE);
        if (null != mListener) {
            mListener.onDragStart();
        }
    }

    private void onDroppedByUser(float x, float y) {
        mHoverView.mScreen.getExitView().setVisibility(HIDE);
//        if (null != mListener) {
//            mListener.onDragEnd();
//        }

        boolean droppedOnExit = mHoverView.mScreen.getExitView().isInExitZone(mFloatingTab.getPosition());
        Logger.getLogger(TAG).severe("droppedOnExit =" + droppedOnExit + "  xx=" + mFloatingTab.getPosition());
        if (droppedOnExit) {
            Logger.getLogger(TAG).severe("User dropped floating tab on exit.");
            closeMenu(new Runnable() {
                @Override
                public void run() {
                    if (null != mHoverView.mOnExitListener) {
                        mHoverView.mOnExitListener.onExit();
                    }
                }
            });
        } else {
            int tabSize = 216;
            Point screenSize = new Point(mHoverView.mScreen.getWidth(), mHoverView.mScreen.getHeight());
            float tabHorizontalPositionPercent = (float) mFloatingTab.getPosition().getPointX() / screenSize.getPointX();
            float tabVerticalPosition = (float) mFloatingTab.getPosition().getPointY() / screenSize.getPointY();
            Logger.getLogger(TAG).severe("Dropped at horizontal " + tabHorizontalPositionPercent + ", vertical " + tabVerticalPosition);
            SideDock.SidePosition sidePosition = new SideDock.SidePosition(
                    tabHorizontalPositionPercent <= 0.5 ? SideDock.SidePosition.LEFT : SideDock.SidePosition.RIGHT,
                    tabVerticalPosition
            );
            mHoverView.mCollapsedDock = new SideDock(
                    mHoverView,
                    tabSize,
                    sidePosition
            );
            mHoverView.saveVisualState();
            Logger.getLogger(TAG).severe("User dropped tab. Sending to new dock: " + mHoverView.mCollapsedDock);


            sendToDock(x, y);
        }
    }

    private void setTranstate(Point point) {
        mUIHandle.postTask(new Runnable() {
            @Override
            public void run() {
                mFloatingTab.setTranslationX(point.getPointX());
                mFloatingTab.setTranslationY(point.getPointY());
            }
        });

    }

    private void onTap() {
        Logger.getLogger(TAG).severe("Floating tab was tapped.");
        expand();
        if (null != mListener) {
            mListener.onTap();
        }
    }

    private void sendToDock(float x, float y) {
        Logger.getLogger(TAG).severe("Sending floating tab to dock.");
        deactivateDragger();
        mFloatingTab.setDock(mHoverView.mCollapsedDock);
        mFloatingTab.dock(x, y, new Runnable() {
            @Override
            public void run() {
                onDocked();
            }
        });
    }

    private void moveToPotion() {
        Point p = mFloatingTab.convertCenterToCorner(mHoverView.mCollapsedDock.position());
        mFloatingTab.moveTo(p, false);
    }

    private void moveToDock() {


        Point dockPosition = mHoverView.mCollapsedDock.sidePosition().calculateDockPosition(
                new Point(mHoverView.mScreen.getWidth(), mHoverView.mScreen.getHeight()),
                mFloatingTab.getTabSize()
        );
        Logger.getLogger(TAG).severe("Moving floating tag to dock. dockPosition =" + dockPosition);
        mFloatingTab.moveTo(dockPosition, false);

    }

    private void initDockPosition() {
        if (null == mHoverView.mCollapsedDock) {
            int tabSize = 216;
            mHoverView.mCollapsedDock = new SideDock(
                    mHoverView,
                    tabSize,
                    new SideDock.SidePosition(SideDock.SidePosition.LEFT, 0.5f)
            );
        }
    }

    private void onDocked() {
        Logger.getLogger(TAG).severe("Docked. Activating dragger.");
        mIsDocked = true;
        activateDragger();

        // We consider ourselves having gone from "collapsing" to "collapsed" upon the very first dock.
        boolean didJustCollapse = !mIsCollapsed;
        mIsCollapsed = true;
        mHoverView.saveVisualState();
        if (null != mListener) {
            if (didJustCollapse) {
                mHoverView.notifyListenersCollapsed();
                mListener.onCollapsed();
            }
            mListener.onDocked();
        }
    }

    private void moveTabTo(@NonNull Point position) {
        mFloatingTab.moveTo(position, false);
    }

    private void closeMenu(final @Nullable Runnable onClosed) {
        mUIHandle.postTask(new Runnable() {
            @Override
            public void run() {
                mHoverView.mScreen.destroyChainedTab(mFloatingTab);

                if (null != onClosed) {
                    onClosed.run();
                }

                close();

                removeFromWindow();
            }
        });
//        mFloatingTab.disappear(new Runnable() {
//            @Override
//            public void run() {
//                mHoverView.mScreen.destroyChainedTab(mFloatingTab);
//
//                if (null != onClosed) {
//                    onClosed.run();
//                }
//
//                close();
//            }
//        });
    }

    private void activateDragger() {
        Logger.getLogger(TAG).severe("activateDragger  . mHoverView.mWindow =" + mHoverView.mWindow);
        if (mHoverView.mDragger instanceof InWindowDragger) {
            InWindowDragger windowDragger = (InWindowDragger) mHoverView.mDragger;
            windowDragger.setHoverContent(mHoverView.mScreen.getContainer());
            windowDragger.setWindow(mHoverView.mWindow);
        }

        Point point;
        if (mFloatingTab.getDockPosition() == null) {
            point = new Point(mFloatingTab.getTabSize() / 2, mFloatingTab.getPosition().getPointY());
        } else {
            point = mFloatingTab.getDockPosition();
        }
        mHoverView.mDragger.activate(mDragListener, point);
    }

    private void deactivateDragger() {
        mHoverView.mDragger.deactivate();
    }

    public interface Listener {
        void onCollapsing();

        void onCollapsed();

        void onDragStart();

        void onDragEnd();

        void onDocked();

        void onTap();

        // TODO: do we need this?
        void onExited();
    }

    private static final class FloatingTabDragListener implements Dragger.DragListener {

        private final HoverViewStateCollapsed mOwner;

        private FloatingTabDragListener(@NonNull HoverViewStateCollapsed owner) {
            mOwner = owner;
        }

        @Override
        public void onPress(float x, float y) {
        }

        @Override
        public void onDragStart(float x, float y) {
            mOwner.onPickedUpByUser();
        }

        @Override
        public void onDragTo(float x, float y) {
            mOwner.moveTabTo(new Point((int) x, (int) y));
        }

        @Override
        public void onReleasedAt(float x, float y) {
            mOwner.onDroppedByUser(x, y);
        }

        @Override
        public void onTap() {
            mOwner.onTap();
        }
    }
}
