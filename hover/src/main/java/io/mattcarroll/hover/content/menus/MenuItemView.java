/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.content.menus;

import io.mattcarroll.hover.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;


/**
 * View that represents a {@link MenuItem} as a list item.
 */
public class MenuItemView extends StackLayout {

    private Text mTitleTextView;

    private String mTitle;

    public MenuItemView(Context context) {
        super(context);
        init();
    }

    public MenuItemView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_menu_item,this,true);

        mTitleTextView = (Text) findComponentById(ResourceTable.Id_textview_title);
    }

    public void setTitle(String title) {
        mTitle = title;
        updateView();
    }

    private void updateView() {
        mTitleTextView.setText(mTitle);
    }
}
