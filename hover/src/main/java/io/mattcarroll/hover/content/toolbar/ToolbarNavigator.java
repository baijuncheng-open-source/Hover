/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.content.toolbar;


import java.io.IOException;
import java.util.Stack;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.Content;
import io.mattcarroll.hover.ResourceTable;
import io.mattcarroll.hover.content.Navigator;
import io.mattcarroll.hover.content.NavigatorContent;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

/**
 * A {@link Navigator} that offers a {@code Toolbar}.
 */
public class ToolbarNavigator extends Navigator implements Content {

    //private Toolbar mToolbar;
    private PixelMapElement mBackArrowDrawable;
    private Stack<NavigatorContent> mContentStack; // TODO: if we extend Navigator then we don't need to implement our own stack
    private StackLayout mContentContainer;
    private DirectionalLayout.LayoutConfig mContentLayoutParams;

    public ToolbarNavigator(Context context) {
        this(context, null);
    }

    public ToolbarNavigator(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_toolbar_navigator, this, true);
        mBackArrowDrawable = createBackArrowDrawable();

        mContentContainer = (StackLayout) findComponentById(ResourceTable.Id_content_container);
        mContentLayoutParams = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        mContentStack = new Stack<>();
    }

    private PixelMapElement createBackArrowDrawable() {
        // Load the desired back-arrow color from the theme that we're using.
        //need todo
//        int[] attrIds = new int[] { R.attr.colorControlNormal };
//        TypedArray attrs = getContext().obtainStyledAttributes(attrIds);
//        int backArrowColor = attrs.getColor(attrs.getIndex(0), 0xFF000000);
//        attrs.recycle();

        // Apply the desired color to the back-arrow icon and return it.

        PixelMapElement backArrowDrawable = null;
        try {
            backArrowDrawable = new PixelMapElement(getContext().getResourceManager().getResource(ResourceTable.Media_ic_arrow_back));
        } catch (NotExistException e) {

        } catch (IOException e) {

        }
        //backArrowDrawable.setColorFilter(backArrowColor, PorterDuff.Mode.SRC_ATOP);
        return backArrowDrawable;
    }

//    public Toolbar getToolbar() {
//        return mToolbar;
//    }

    @Override
    public void pushContent(@NonNull NavigatorContent content) {
        // Remove the currently visible content (if there is any).
        if (!mContentStack.isEmpty()) {
            mContentContainer.removeComponent(mContentStack.peek().getView());
            mContentStack.peek().onHidden();
        }

        // Push and display the new page.
        mContentStack.push(content);
        showContent(content);

        updateToolbarBackButton();
    }

    @Override
    public boolean popContent() {
        if (mContentStack.size() > 1) {
            // Remove the currently visible content.
            removeCurrentContent();

            // Add back the previous content (if there is any).
            if (!mContentStack.isEmpty()) {
                showContent(mContentStack.peek());
            }

            updateToolbarBackButton();

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void clearContent() {
        if (mContentStack.isEmpty()) {
            // Nothing to clear.
            return;
        }

        // Pop every content View that we can.
        boolean didPopContent = popContent();
        while (didPopContent) {
            didPopContent = popContent();
        }

        // Clear the root View.
        removeCurrentContent();
    }

    private void showContent(@NonNull NavigatorContent content) {
        mContentContainer.addComponent(content.getView(), mContentLayoutParams);
        content.onShown(this);
    }

    private void removeCurrentContent() {
        NavigatorContent visibleContent = mContentStack.pop();
        mContentContainer.removeComponent(visibleContent.getView());
        visibleContent.onHidden();
    }

    private void updateToolbarBackButton() {
        if (mContentStack.size() >= 2) {
            // Show the back button.
            //mToolbar.setNavigationIcon(mBackArrowDrawable);
        } else {
            // Hide the back button.
            //mToolbar.setNavigationIcon(null);
        }
    }

    @NonNull
    @Override
    public Component getView() {
        return this;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onShown() {
        // Do nothing.
    }

    @Override
    public void onHidden() {
        // Do nothing.
    }

}
