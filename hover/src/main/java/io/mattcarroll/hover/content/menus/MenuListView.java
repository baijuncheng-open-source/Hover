/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.content.menus;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * View that displays all items in a given {@link Menu}.
 */
public class MenuListView extends StackLayout {

    private ListContainer mListView;
    private Component mEmptyView;
    private MenuListAdapter mMenuListAdapter;
    private MenuItemSelectionListener mMenuItemSelectionListener;

    public MenuListView(Context context) {
        this(context, null);
    }

    public MenuListView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mMenuListAdapter = new MenuListAdapter();

        mListView = new ListContainer(getContext());
        mListView.setItemProvider(mMenuListAdapter);
        mListView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                if (null != mMenuItemSelectionListener) {
                    mMenuItemSelectionListener.onMenuItemSelected(mMenuListAdapter.getItem(position));
                }
            }
        });

        addComponent(mListView, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
    }

    public void setEmptyView(@Nullable Component emptyView) {
        // Remove existing empty view.
        if (null != mEmptyView) {
            removeComponent(mEmptyView);
        }

        mEmptyView = emptyView;
        if (null != mEmptyView) {
            addComponent(mListView, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        }

        updateEmptyViewVisibility();
    }

    public void setMenu(@Nullable Menu menu) {
        mMenuListAdapter.setMenu(menu);
        updateEmptyViewVisibility();
    }

    public void setMenuItemSelectionListener(@Nullable MenuItemSelectionListener listener) {
        mMenuItemSelectionListener = listener;
    }

    private void updateEmptyViewVisibility() {
        boolean isEmpty = null == mMenuListAdapter || 0 == mMenuListAdapter.getCount();
        if (null != mEmptyView) {
            mEmptyView.setVisibility(isEmpty ? VISIBLE : HIDE);
            mListView.setVisibility(isEmpty ? HIDE : VISIBLE);
        } else {
            mListView.setVisibility(VISIBLE);
        }
    }

    public interface MenuItemSelectionListener {
        void onMenuItemSelected(@NonNull MenuItem menuItem);
    }
}
