/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import java.util.logging.Logger;

import static ohos.agp.components.DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM;
import static ohos.agp.components.DependentLayout.LayoutConfig.ALIGN_PARENT_TOP;

/**
 * Rectangular area that displays {@link Content}.  A {@code ContentDisplay} also renders a caret
 * that points at a tab.
 */
class ContentDisplay extends DependentLayout {

    private static final String TAG = "ContentDisplay";

    private Component mContainer;
    private StackLayout mContentView;
    private ShapeElement mContentBackground;
    private TabSelectorView mTabSelectorView;
    private FloatingTab mSelectedTab;
    private Content mContent;
    private boolean mIsVisible = false;

    private final ComponentTreeObserver.GlobalLayoutListener mMyVisibilityWatcher = new ComponentTreeObserver.GlobalLayoutListener() {
        @Override
        public void onGlobalLayoutUpdated() {
            if (mIsVisible && VISIBLE != getVisibility()) {
                mIsVisible = false;
                // Went from visible to not-visible. Hide tab selector to avoid visual artifacts
                // when we appear again.
                mTabSelectorView.setVisibility(INVISIBLE);
            } else {
                mIsVisible = true;
            }
        }
    };

    private final FloatingTab.OnPositionChangeListener mOnTabPositionChangeListener = new FloatingTab.OnPositionChangeListener() {
        @Override
        public void onPositionChange(@NonNull Point position) {
            Logger.getLogger(TAG).severe(mSelectedTab + " tab moved to " + position);
            updateTabSelectorPosition();

            setPadding(0, (int)position.getPointY() + (mSelectedTab.getTabSize() / 2), 0, 0);

            // We have received an affirmative position for the selected tab. Show tab selector.
            mTabSelectorView.setVisibility(VISIBLE);
        }

        @Override
        public void onDockChange(@NonNull Point dock) {
            // No-op.
        }
    };

    public ContentDisplay(@NonNull Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_hover_menu_content,this,true);

        mContainer = findComponentById(ResourceTable.Id_container);
        expandToScreenBounds();

        int backgroundCornerRadiusPx = 21;
        mTabSelectorView = (TabSelectorView) findComponentById(ResourceTable.Id_tabselector);
        mTabSelectorView.setPadding(backgroundCornerRadiusPx, 0, backgroundCornerRadiusPx, 0);

        mContentView = (StackLayout) findComponentById(ResourceTable.Id_view_content_container);

        mContentBackground = new ShapeElement(getContext(),ResourceTable.Graphic_round_rect_white);
        mContentView.setBackground(mContentBackground);
        getComponentTreeObserver().addTreeLayoutChangedListener(mMyVisibilityWatcher);
    }

    @Override
    public boolean isBoundToWindow() {
        ComponentContainer.LayoutConfig layoutParams = getLayoutConfig();
        layoutParams.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutParams.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
        setLayoutConfig(layoutParams);
        return super.isBoundToWindow();
    }

    public void enableDebugMode(boolean debugMode) {
        ShapeElement shapeElement = new ShapeElement();
        if (debugMode) {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("0x88FFFF00")));
        } else {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));

        }
        setBackground(shapeElement);
    }

    public void selectedTabIs(@Nullable FloatingTab tab) {
        // Disconnect from old selected tab.
        if (null != mSelectedTab) {
            mSelectedTab.removeOnPositionChangeListener(mOnTabPositionChangeListener);
        }

        mSelectedTab = tab;

        // Connect to new selected tab.
        if (null != mSelectedTab) {
            updateTabSelectorPosition();
            mSelectedTab.addOnPositionChangeListener(mOnTabPositionChangeListener);
        } else {
            mTabSelectorView.setVisibility(INVISIBLE);
        }
    }

    private void updateTabSelectorPosition() {
        Point tabPosition = mSelectedTab.getPosition();
        Logger.getLogger(TAG).severe("Updating tab position to " + tabPosition.getPointX());
        mTabSelectorView.setSelectorPosition((int)tabPosition.getPointX());
    }

    public void displayContent(@Nullable Content content) {
        Logger.getLogger(TAG).severe("displayContent content =" + content);
        if (content == mContent) {
            // If content hasn't changed then we don't need to do anything.
            return;
        }

        if (null != mContent) {
            Logger.getLogger(TAG).severe("mContent.getView() =" + mContent.getView());
            mContentView.removeComponent(mContent.getView());
            mContent.onHidden();
        }

        mContent = content;
        if (null != mContent) {
            mContentView.addComponent(mContent.getView());
            mContent.onShown();

            if (content.isFullscreen()) {
                expandToScreenBounds();
            } else {
                wrapContent();
            }
        }
    }

    public void expandToScreenBounds() {
        DependentLayout.LayoutConfig layoutParams = (DependentLayout.LayoutConfig) mContainer.getLayoutConfig();
        layoutParams.height = 0;
        layoutParams.addRule(ALIGN_PARENT_TOP);
        layoutParams.addRule(ALIGN_PARENT_BOTTOM);
        mContainer.setLayoutConfig(layoutParams);
    }

    public void wrapContent() {
        DependentLayout.LayoutConfig layoutParams = (LayoutConfig) mContainer.getLayoutConfig();
        layoutParams.height = LayoutConfig.MATCH_CONTENT;
        layoutParams.addRule(ALIGN_PARENT_TOP);
        layoutParams.addRule(ALIGN_PARENT_BOTTOM, 0); // This means "remove rule". Can't use removeRule() until API 17.
        mContainer.setLayoutConfig(layoutParams);
    }
}
