/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import io.mattcarroll.hover.AnnotationDef.ColorInt;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * {@code View} that draws a triangle selector icon at a given horizontal position within its bounds.
 * A {@code TabSelectorView} is like a horizontal rail upon which its triangle selector can slide
 * left/right.
 *
 * Class is public to allow for XML use.
 */
public class TabSelectorView extends Component implements Component.EstimateSizeListener, Component.LayoutRefreshedListener {

    private static final String TAG = "HoverMenuTabSelectorView";

    private static final int DEFAULT_SELECTOR_WIDTH_DP = 24;
    private static final int DEFAULT_SELECTOR_HEIGHT_DP = 16;

    private int mSelectorWidthPx;
    private int mSelectorHeightPx;
    private int mDesiredSelectorCenterLocationPx; // the selector position that the client wants
    private int mLeftMostSelectorLocationPx; // based on mLeftBoundOffset and mSelectorWidthPx;
    private int mRightMostSelectorLocationPx; // based on mRightBoundOffsetPx and mSelectorWidthPx;

    private Path mSelectorPaintPath;
    private Paint mSelectorPaint;

    public TabSelectorView(Context context) {
        this(context, null);
    }

    public TabSelectorView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        int density = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().densityDpi;
        mSelectorWidthPx = (int) DEFAULT_SELECTOR_WIDTH_DP * density;
        mSelectorHeightPx = (int) DEFAULT_SELECTOR_HEIGHT_DP * density;
        setSelectorPosition(mSelectorWidthPx / 2);

        mSelectorPaint = new Paint();
        mSelectorPaint.setColor(new Color(Color.getIntColor("#3b3b3b")));
        mSelectorPaint.setStyle(Paint.Style.FILL_STYLE);
        setEstimateSizeListener(this::onEstimateSize);
        setLayoutRefreshedListener(this::onRefreshed);
        addDrawTask(new DrawTab());
    }

    public void setSelectorColor(@ColorInt int color) {
        mSelectorPaint.setColor(new Color(color));
        invalidate();
    }

    /**
     * Sets the pixel position of the center of the selector icon. The position given will be
     * clamped to available space in this View.
     *
     * @param position horizontal pixel position
     */
    public void setSelectorPosition(int position) {
        mDesiredSelectorCenterLocationPx = position;
        invalidateSelectorPath();
    }


    private void invalidateSelectorPath() {
        mLeftMostSelectorLocationPx = getPaddingLeft() + (mSelectorWidthPx / 2);
        mRightMostSelectorLocationPx = getWidth() - getPaddingRight() - (mSelectorWidthPx / 2);

        int selectorCenterLocationPx = clampSelectorPosition(mDesiredSelectorCenterLocationPx);

        mSelectorPaintPath = new Path();
        mSelectorPaintPath.moveTo(selectorCenterLocationPx, 0); // top of triangle
        mSelectorPaintPath.lineTo(selectorCenterLocationPx + (mSelectorWidthPx / 2), mSelectorHeightPx); // bottom right of triangle
        mSelectorPaintPath.lineTo(selectorCenterLocationPx - (mSelectorWidthPx / 2), mSelectorHeightPx); // bottom left of triangle
        mSelectorPaintPath.lineTo(selectorCenterLocationPx, 0); // back to origin

        invalidate();
    }

    private int clampSelectorPosition(int position) {
        if (position < mLeftMostSelectorLocationPx) {
            return mLeftMostSelectorLocationPx;
        } else if (position > mRightMostSelectorLocationPx) {
            return mRightMostSelectorLocationPx;
        } else {
            return position;
        }
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        setEstimatedSize(MeasureSpec.getSize(i), mSelectorHeightPx);
        return false;
    }

    @Override
    public void onRefreshed(Component component) {
        invalidateSelectorPath();
    }

    private class DrawTab implements DrawTask {

        @Override
        public void onDraw(Component component, Canvas canvas) {
            canvas.drawPath(mSelectorPaintPath, mSelectorPaint);
        }
    }
}
