/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.window;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import io.mattcarroll.hover.HoverView;
import io.mattcarroll.hover.OnExitListener;
import io.mattcarroll.hover.SideDock;
import io.mattcarroll.hover.overlay.OverlayPermission;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.rpc.IRemoteObject;
import ohos.utils.system.SystemCapability;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * {@code Service} that presents a {@link HoverView} within a {@code Window}.
 * <p>
 * The {@code HoverView} is displayed whenever any Intent is received by this {@code Service}. The
 * {@code HoverView} is removed and destroyed whenever this {@code Service} is destroyed.
 * <p>
 * A {@link Ability} is required for displaying a {@code HoverView} in a {@code Window} because there
 * is no {@code Activity} to associate with the {@code HoverView}'s UI. This {@code Service} is the
 * application's link to the device's {@code Window} to display the {@code HoverView}.
 */
public abstract class HoverMenuService extends Ability {

    private static final String TAG = "HoverMenuService";

    private HoverView mHoverView;
    private boolean mIsRunning;
    private OnExitListener mOnMenuOnExitListener = new OnExitListener() {
        @Override
        public void onExit() {
            Logger.getLogger(TAG).severe("Menu exit requested. Exiting. mHoverView=" + mHoverView + "  mHoverView.s=" + mHoverView.mState);
            mHoverView.removeFromWindow();
            onHoverMenuExitingByUserRequest();
            onStop();
        }
    };

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Logger.getLogger(TAG).severe("onCreate()");
        SystemCapability.Notification foregroundNotification = getForegroundNotification();
        if (null != foregroundNotification) {
            int notificationId = getForegroundNotificationId();
            //need todo
            //startForeground(notificationId, foregroundNotification);
        }
    }

    @Override
    protected void onCommand(Intent intent, boolean restart) {
        super.onCommand(intent, restart);
        Logger.getLogger(TAG).severe("onCommand");
        // Stop and return immediately if we don't have permission to display things above other
        // apps.
        if (!OverlayPermission.hasRuntimePermissionToDrawOverlay(getApplicationContext())) {
            Logger.getLogger(TAG).severe("Cannot display a Hover menu in a Window without the draw overlay permission.");
            onStop();
            return;
        }

        if (null == intent) {
            Logger.getLogger(TAG).severe("Received null Intent. Not creating Hover menu.");
            onStop();
            return;
        }

        if (!mIsRunning) {
            Logger.getLogger(TAG).severe("onStartCommand() - showing Hover menu.");
            mIsRunning = true;

            initHoverMenu(intent);
        }
    }

    private PixelMapElement getPixelMapElement(int resId) {
        PixelMapElement element = null;
        try {
            element = new PixelMapElement(getResourceManager().getResource(resId));
        } catch (NotExistException e) {

        } catch (IOException e) {

        }
        return element;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.getLogger(TAG).severe("onStop()");
        if (mIsRunning) {
            mHoverView.removeFromWindow();
            mIsRunning = false;
        }
    }

    @Nullable
    @Override
    protected IRemoteObject onConnect(Intent intent) {
        Logger.getLogger(TAG).severe("onConnect");
        return super.onConnect(intent);
    }


    private void initHoverMenu(@NonNull Intent intent) {
        mHoverView = HoverView.createForWindow(
                this,
                new WindowViewController(WindowManager.getInstance()),
                new SideDock.SidePosition(SideDock.SidePosition.LEFT, 0.5f)
        );
        mHoverView.setOnExitListener(mOnMenuOnExitListener);
        mHoverView.addToWindow();

        onHoverMenuLaunched(intent, mHoverView);


    }

    /**
     * Hook for subclasses to return a custom Context to be used in the creation of the {@code HoverMenu}.
     * For example, subclasses might choose to provide a ContextThemeWrapper.
     *
     * @return context for HoverMenu initialization
     */
    protected Context getContextForHoverMenu() {
        return this;
    }

    @NonNull
    protected HoverView getHoverView() {
        return mHoverView;
    }

    protected int getForegroundNotificationId() {
        // Subclasses should provide their own notification ID if using a notification.
        return 123456789;
    }

    @Nullable
    protected SystemCapability.Notification getForegroundNotification() {
        // If subclass returns a non-null Notification then the Service will be run in
        // the foreground.
        return null;
    }

    protected void onHoverMenuLaunched(@NonNull Intent intent, @NonNull HoverView hoverView) {
        // Hook for subclasses.
    }

    /**
     * Hook method for subclasses to take action when the user exits the HoverMenu. This method runs
     * just before this {@code HoverMenuService} calls {@code stopSelf()}.
     */
    protected void onHoverMenuExitingByUserRequest() {
        // Hook for subclasses.
    }
}
