/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.window;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import java.util.logging.Logger;

import static ohos.agp.window.service.WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS;

/**
 * Controls {@code View}s' positions, visibility, etc within a {@code Window}.
 */
public class WindowViewController {

    private WindowManager mWindowManager;
    private static final String TAG = "WindowViewController";

    public WindowViewController(@NonNull WindowManager windowManager) {
        mWindowManager = windowManager;
        Logger.getLogger(TAG).severe("WindowViewController init");
    }

    public Window  addView(Context context, int width, int height, boolean isTouchable, @NonNull ComponentContainer view) {
        // If this view is untouchable then add the corresponding flag, otherwise set to zero which
        // won't have any effect on the OR'ing of flags.
        int touchableFlag = isTouchable ? 0 : WindowManager.LayoutConfig.MARK_TOUCHABLE_IMPOSSIBLE;
        int windowType = WindowManager.LayoutConfig.MOD_APPLICATION_OVERLAY;

        WindowManager.LayoutConfig params = new WindowManager.LayoutConfig();
        params.width = width;
        params.height = height;
        params.type = windowType;
        params.flags = WindowManager.LayoutConfig.MARK_FOCUSABLE_IMPOSSIBLE | touchableFlag | MARK_TRANSLUCENT_STATUS;
        params.alignment = LayoutAlignment.TOP | LayoutAlignment.LEFT;
        params.x = 0;
        params.y = 0;


        Window window = mWindowManager.addComponent(view, context, WindowManager.LayoutConfig.MOD_APPLICATION_OVERLAY);
        window.setLayoutConfig(params);

        window.setTransparent(true);
        Logger.getLogger(TAG).severe("addView  new window  width =" + width + "  height =" + height + " window=" + window);
        return window;
    }

    public void removeView(@NonNull Component view, Window window) {
        Logger.getLogger(TAG).severe("removeView  view=" + view + " window=" + window);
        if (window != null) {
            mWindowManager.destroyWindow(window);
        }
    }

    public Point getViewPosition(@NonNull Component view) {
        int[] p = view.getLocationOnScreen();
        return new Point(p[0], p[1]);
    }

    public void moveViewTo(Component view, int x, int y, Window window) {
        WindowManager.LayoutConfig params = window.getLayoutConfig().get();
        params.x = x;
        params.y = y;
        window.setLayoutConfig(params);
    }

    public void showView(Component view) {
        try {
            //no use code
//            WindowManager.LayoutConfig params = (WindowManager.LayoutConfig) view.getLayoutConfig();
//            mWindowManager.addComponent(view, params)
        } catch (IllegalStateException e) {
            // The view is already visible.
        }
    }

    public void hideView(Component view) {
        try {
            //need todo
            //mWindowManager.removeView(view);
        } catch (IllegalArgumentException e) {
            // The View wasn't visible to begin with.
        }
    }

    public void makeTouchable(Component view, Window window) {
        WindowManager.LayoutConfig params = window.getLayoutConfig().get();
        params.flags = params.flags & ~WindowManager.LayoutConfig.MARK_TOUCHABLE_IMPOSSIBLE & ~WindowManager.LayoutConfig.MARK_FOCUSABLE_IMPOSSIBLE;
        window.setLayoutConfig(params);
    }

    public void makeUntouchable(Component view, Window window) {
        WindowManager.LayoutConfig params = window.getLayoutConfig().get();
        params.flags = params.flags | WindowManager.LayoutConfig.MARK_TOUCHABLE_IMPOSSIBLE | WindowManager.LayoutConfig.MARK_FOCUSABLE_IMPOSSIBLE;
        window.setLayoutConfig(params);
    }

}
