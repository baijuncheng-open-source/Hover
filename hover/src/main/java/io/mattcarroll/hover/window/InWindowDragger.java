/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover.window;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.Dragger;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Window;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.logging.Logger;

/**
 * {@link Dragger} implementation that works within a {@code Window}.
 */
public class InWindowDragger implements Dragger {

    private static final String TAG = "InWindowDragger";

    private final Context mContext;
    private final WindowViewController mWindowViewController;
    private final int mTouchAreaDiameter;
    private final float mTapTouchSlop;
    private Component mDragView;
    private Dragger.DragListener mDragListener;
    private boolean mIsActivated;
    private boolean mIsDragging;
    private boolean mIsDebugMode;
    private Window mWindow;
    private Window mBallWindow;

    private Point mOriginalViewPosition = new Point();
    private Point mCurrentViewPosition = new Point();
    private Point mOriginalTouchPosition = new Point();
    private Component mHoverContent;
    private Point mDragViewPosition = new Point();

    private Component.TouchEventListener mDragTouchListener = new Component.TouchEventListener() {
        @Override
        public boolean onTouchEvent(Component component, TouchEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    mIsDragging = false;

                    mOriginalViewPosition = getDragViewCenterPosition();
                    mCurrentViewPosition = new Point(mOriginalViewPosition.getPointX(), mOriginalViewPosition.getPointY());
                    mOriginalTouchPosition.modify(motionEvent.getPointerScreenPosition(0).getX(), motionEvent.getPointerScreenPosition(0).getY());

                    mDragListener.onPress(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                    Logger.getLogger(TAG).severe("ACTION_DOWN  mOriginalViewPosition=" + mOriginalViewPosition + "  mCurrentViewPosition=" + mCurrentViewPosition + "  mOriginalTouchPosition=" + mOriginalTouchPosition);

                    return true;
                case TouchEvent.POINT_MOVE:
                    float dragDeltaX = motionEvent.getPointerScreenPosition(0).getX() - mOriginalTouchPosition.getPointX();
                    float dragDeltaY = motionEvent.getPointerScreenPosition(0).getY() - mOriginalTouchPosition.getPointY();
                    mCurrentViewPosition = new Point(
                            mOriginalViewPosition.getPointX() + dragDeltaX,
                            mOriginalViewPosition.getPointY() + dragDeltaY
                    );
                    Logger.getLogger(TAG).severe("ACTION_MOVE  mOriginalViewPosition=" + mOriginalViewPosition + "  mCurrentViewPosition=" + mCurrentViewPosition + "  mOriginalTouchPosition=" + mOriginalTouchPosition + "  dragDeltaX=" + dragDeltaX);

                    if (mIsDragging || !isTouchWithinSlopOfOriginalTouch(dragDeltaX, dragDeltaY)) {
                        if (!mIsDragging) {
                            // Dragging just started
                            Logger.getLogger(TAG).severe(" MOVE Start Drag.   x=" + mCurrentViewPosition.getPointX());
                            mIsDragging = true;
                            mDragListener.onDragStart(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                        } else {
                            moveDragViewTo(mCurrentViewPosition);
                            mDragListener.onDragTo(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                        }
                    }

                    return true;
                case TouchEvent.PRIMARY_POINT_UP:
                    if (!mIsDragging) {
                        Logger.getLogger(TAG).severe("Reporting as a tap.");
                        mDragListener.onTap();
                    } else {
                        Logger.getLogger(TAG).severe("Reporting as a drag release at: " + mCurrentViewPosition);
                        mDragListener.onReleasedAt(mCurrentViewPosition.getPointX(), mCurrentViewPosition.getPointY());
                    }

                    return true;
                default:
                    return false;
            }
        }
    };

    /**
     * Note: {@code view} must already be added to the {@code Window}.
     *
     * @param context              context
     * @param windowViewController windowViewController
     * @param tapTouchSlop         tapTouchSlop
     */
    public InWindowDragger(@NonNull Context context,
                           @NonNull WindowViewController windowViewController,
                           int touchAreaDiameter,
                           float tapTouchSlop) {
        mContext = context;
        mWindowViewController = windowViewController;
        mTouchAreaDiameter = touchAreaDiameter;
        mTapTouchSlop = tapTouchSlop;
    }

    public void setHoverContent(Component hoverContent) {
        mHoverContent = hoverContent;
    }

    public void setWindow(Window window) {
        mWindow = window;
    }

    public void activate(@NonNull DragListener dragListener, @NonNull Point dragStartCenterPosition) {
        if (!mIsActivated) {
            Logger.getLogger(TAG).severe("window Activating.");
            createTouchControlView(dragStartCenterPosition);

            mDragListener = dragListener;
            mDragView.setTouchEventListener(mDragTouchListener);
            mIsActivated = true;

        }
    }

    public void deactivate() {
        if (mIsActivated) {
            Logger.getLogger(TAG).severe("Deactivating.");
            mDragView.setTouchEventListener(null);
            destroyTouchControlView();
            mIsActivated = false;
        }
    }

    @Override
    public void enableDebugMode(boolean isDebugMode) {
        mIsDebugMode = isDebugMode;
        updateTouchControlViewAppearance();
    }

    private void createTouchControlView(@NonNull final Point dragStartCenterPosition) {
        mDragView = new Component(mContext);
        DirectionalLayout directionalLayout = new DirectionalLayout(mContext);
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig();
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        directionalLayout.setLayoutConfig(layoutConfig);
        directionalLayout.setOrientation(Component.VERTICAL);

        directionalLayout.addComponent(mDragView);
        mBallWindow = mWindowViewController.addView(mContext, mTouchAreaDiameter, mTouchAreaDiameter, true, directionalLayout);
        mWindowViewController.moveViewTo(mDragView, (int) dragStartCenterPosition.getPointX() - (mTouchAreaDiameter / 2), (int) dragStartCenterPosition.getPointY() - (mTouchAreaDiameter / 2), mBallWindow);

        mDragViewPosition = new Point((int) dragStartCenterPosition.getPointX() - (mTouchAreaDiameter / 2), (int) dragStartCenterPosition.getPointY() - (mTouchAreaDiameter / 2));
        mDragView.setTouchEventListener(mDragTouchListener);

        updateTouchControlViewAppearance();
    }

    private void destroyTouchControlView() {
        if (mBallWindow != null) {
            mWindowViewController.removeView(mDragView, mBallWindow);
            mDragView = null;
        }
    }

    private void updateTouchControlViewAppearance() {
        if (null != mDragView) {
            ShapeElement element = new ShapeElement();
            if (mIsDebugMode) {
                element.setRgbColor(new RgbColor(Color.getIntColor("#44FF0000")));
            } else {
                element.setRgbColor(new RgbColor(Color.getIntColor("#00000000")));
            }
            mDragView.setBackground(element);
        }
    }

    private boolean isTouchWithinSlopOfOriginalTouch(float dx, float dy) {
        double distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        Logger.getLogger(TAG).severe("Drag distance " + distance + " vs slop allowance " + mTapTouchSlop);
        return distance < mTapTouchSlop;
    }

    private Point getDragViewCenterPosition() {
        return mDragViewPosition;
    }

    public void moveDragViewTo(Point centerPosition) {
        Logger.getLogger(TAG).severe("before,Center position: " + centerPosition);
        Point cornerPosition = convertCenterToCorner(centerPosition);
        Logger.getLogger(TAG).severe("after,Corner position: " + cornerPosition);
        mWindowViewController.moveViewTo(mDragView, (int) cornerPosition.getPointX(), (int) cornerPosition.getPointY(), mBallWindow);
        mDragViewPosition = new Point((int) cornerPosition.getPointX(), (int) cornerPosition.getPointY());
    }

    private Point convertCornerToCenter(@NonNull Point cornerPosition) {
        return new Point(
                cornerPosition.getPointX() + (216 / 2),
                cornerPosition.getPointY() + (216 / 2)
        );
    }

    private Point convertCenterToCorner(@NonNull Point centerPosition) {
        return new Point(
                centerPosition.getPointX() - (mDragView.getWidth() / 2),
                centerPosition.getPointY() - (mDragView.getHeight() / 2)
        );
    }
}
