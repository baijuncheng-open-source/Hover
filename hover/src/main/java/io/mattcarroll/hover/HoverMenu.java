/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@code HoverMenu} models the structure of a menu that appears within a {@link HoverView}.
 *
 * A {@code HoverMenu} includes an ordered list of {@link Section}s.  Each {@code Section} has a tab
 * {@code View} that represents the section, and the {@link Content} of the given section.
 */
public abstract class HoverMenu {

    private List<Section> mSections = new ArrayList<>();

    public abstract String getId();

    public abstract int getSectionCount();

    @Nullable
    public abstract Section getSection(int index);

    @Nullable
    public abstract Section getSection(@NonNull SectionId sectionId);

    public int getSectionIndex(@NonNull Section section) {
        for (int i = 0; i < mSections.size(); ++i) {
            if (section.equals(mSections.get(i))) {
                return i;
            }
        }
        return -1;
    }

    @NonNull
    public abstract List<Section> getSections();


    public void notifyMenuChanged() {
        List<Section> oldSections = mSections;
        List<Section> newSections = getSections();
        mSections = newSections;
    }

    public static class SectionId {

        private String mId;

        public SectionId(@NonNull String id) {
            mId = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SectionId sectionId = (SectionId) o;

            return mId.equals(sectionId.mId);

        }

        @Override
        public int hashCode() {
            return mId.hashCode();
        }

        @Override
        public String toString() {
            return mId;
        }
    }

    public static class Section {

        private final SectionId mId;
        private final Component mTabView;
        private final Content mContent;

        public Section(@NonNull SectionId id, @NonNull Component tabView, @NonNull Content content) {
            mId = id;
            mTabView = tabView;
            mContent = content;
        }

        @NonNull
        public SectionId getId() {
            return mId;
        }

        @NonNull
        public Component getTabView() {
            return mTabView;
        }

        @NonNull
        public Content getContent() {
            return mContent;
        }
    }
}
