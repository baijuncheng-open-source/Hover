/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;

import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.logging.Logger;

/**
 * {@code FloatingTab} is the cornerstone of a {@link HoverView}.  When a {@code HoverView} is
 * collapsed, it is reduced to a single {@code FloatingTab} that the user can drag and drop.  When
 * a {@code HoverView} is expanded, that one {@code FloatingTab} slides to a row of tabs that appear
 * and offer a menu system.
 * <p>
 * A {@code FloatingTab} can move around the screen in various ways. A {@code FloatingTab} can place
 * itself at a "dock position", or slide from its current position to its "dock position", or
 * position itself at an arbitrary location on screen.
 * <p>
 * {@code FloatingTab}s position themselves based on their center.
 */
public class FloatingTab extends StackLayout implements ComponentTreeObserver.WindowBoundListener {

    private static final String TAG = "FloatingTab";

    private final String mId;
    private int mTabSize;
    private Component mTabView;
    private Dock mDock;
    private final Set<OnPositionChangeListener> mOnPositionChangeListeners = new CopyOnWriteArraySet<>();

    private EventHandler mUIhandler = new EventHandler(EventRunner.getMainEventRunner());

    private final LayoutRefreshedListener mOnLayoutChangeListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            notifyListenersOfPositionChange();
        }
    };

    public FloatingTab(@NonNull Context context, @NonNull String tabId, boolean isExpend) {
        super(context);
        mId = tabId;
        mTabSize = 216;

        int padding = 24;
        setPadding(padding, padding, padding, padding);
        setEstimatedSize(mTabSize, mTabSize);
        if (isExpend) {
            // Make this View the desired size.
            ComponentContainer.LayoutConfig layoutParams = getLayoutConfig();
            layoutParams.width = mTabSize;
            layoutParams.height = mTabSize;
            setLayoutConfig(layoutParams);
        }
    }

    @Override
    public void onWindowBound() {

        setLayoutRefreshedListener(mOnLayoutChangeListener);
    }

    @Override
    public void onWindowUnbound() {
        setLayoutRefreshedListener(null);
    }

    public void appear(@Nullable final Runnable onAppeared) {
        AnimatorGroup animatorGroup = new AnimatorGroup();
        AnimatorProperty scaleX = new AnimatorProperty();
        scaleX.scaleXFrom(0.0f);
        scaleX.scaleX(1.0f);
        scaleX.setDuration(250);

        AnimatorProperty scaleY = new AnimatorProperty();
        scaleY.scaleYFrom(0.0f);
        scaleY.scaleY(1.0f);
        scaleY.setDuration(250);
        animatorGroup.runParallel(scaleX, scaleY);
        animatorGroup.start();

        animatorGroup.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (null != onAppeared) {
                    mUIhandler.postTask(onAppeared);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        setVisibility(VISIBLE);
    }

    public void appearImmediate() {
        setVisibility(VISIBLE);
    }

    public void disappear(@Nullable final Runnable onDisappeared) {
        if (null != onDisappeared) {
            mUIhandler.postTask(onDisappeared);
        }

        AnimatorGroup animatorGroup = new AnimatorGroup();
        AnimatorProperty scaleX = new AnimatorProperty();
        scaleX.scaleX(0.0f);
        scaleX.setDuration(250);

        AnimatorProperty scaleY = new AnimatorProperty();
        scaleY.scaleY(0.0f);
        scaleY.setDuration(250);
        animatorGroup.runParallel(scaleX, scaleY);
        animatorGroup.start();

        animatorGroup.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                setVisibility(HIDE);

//                if (null != onDisappeared) {
//                    mUIhandler.postTask(onDisappeared);
//                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
    }

    public void disappearImmediate() {
        setVisibility(HIDE);
    }

    @NonNull
    public String getTabId() {
        return mId;
    }

    public int getTabSize() {
        return mTabSize;
    }

    public void setTabView(@Nullable Component view) {
        if (view == mTabView) {
            // If Tab View hasn't changed, no need to do anything.
            return;
        }

        removeAllComponents();

        mTabView = view;
        if (null != mTabView) {
            StackLayout.LayoutConfig layoutParams = new StackLayout.LayoutConfig(
                    ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT
            );
            addComponent(mTabView, layoutParams);
        }
    }

    public Component getTabView() {
        return mTabView;
    }

    // Returns the center position of this tab.
    @NonNull
    public Point getPosition() {
        return new Point(
                (int) (getTranslationX() + (getTabSize() / 2)),
                (int) (getTranslationY() + (getTabSize() / 2))
        );
    }

    @Nullable
    public Point getDockPosition() {
        if(mDock == null) {
            return null;
        }
        return mDock.position();
    }

    public void setDock(@NonNull Dock dock) {
        mDock = dock;
        notifyListenersOfDockChange();
    }

    public void dock() {
        dock(0, 0, null);
    }

    public void dock(float x, float y, @Nullable final Runnable onDocked) {
        Point destinationCornerPosition = convertCenterToCorner(mDock.position());
        float fromX = x;
        float toX = destinationCornerPosition.getPointX();
        if (toX < 0) {
            toX = 0;
        }

        float fromY = y;
        float toY = destinationCornerPosition.getPointY();
        if (toY < 0) {
            toY = -toY;
        }

        AnimatorProperty animatorPropertx = new AnimatorProperty();
        animatorPropertx.setCurveType(Animator.CurveType.ANTICIPATE_OVERSHOOT);
        animatorPropertx.moveFromX(fromX);
        animatorPropertx.moveToX(toX);

        animatorPropertx.setDuration(500);
        animatorPropertx.setTarget(this);
        animatorPropertx.start();

        if (fromY == 0) {
            AnimatorProperty animatorPropertyY = new AnimatorProperty();
            animatorPropertyY.setCurveType(Animator.CurveType.ANTICIPATE_OVERSHOOT);
            animatorPropertyY.moveFromY(fromY);
            animatorPropertyY.moveToY(toY);

            animatorPropertyY.setDuration(500);
            animatorPropertyY.setTarget(this);
            animatorPropertyY.start();
        }

        if (onDocked != null) {
            mUIhandler.postTask(onDocked, 500);
            notifyListenersOfPositionChange();
        }
    }

    public void dockImmediately() {
        moveTo(mDock.position(), false);
    }

    public void moveTo(@NonNull Point floatPosition, boolean fromChain) {
        Point cornerPosition = convertCenterToCorner(floatPosition);
        float x = cornerPosition.getPointX();
        if (x < 0) {
            x = 0;
        }
        float y = cornerPosition.getPointY();
        if (y < 0) {
            y = -cornerPosition.getPointY();
        }
        setTranslationX(x);
        setTranslationY(y);
    }

    public Point convertCenterToCorner(@NonNull Point centerPosition) {
        return new Point(
                centerPosition.getPointX() - (getTabSize() / 2),
                centerPosition.getPointY() - (getTabSize() / 2)
        );
    }

    public void addOnPositionChangeListener(@Nullable OnPositionChangeListener listener) {
        mOnPositionChangeListeners.add(listener);
    }

    public void removeOnPositionChangeListener(@NonNull OnPositionChangeListener listener) {
        mOnPositionChangeListeners.remove(listener);
    }

    private void notifyListenersOfPositionChange() {
        Point position = getPosition();
        for (OnPositionChangeListener listener : mOnPositionChangeListeners) {
            listener.onPositionChange(position);
        }
    }

    private void notifyListenersOfDockChange() {
        for (OnPositionChangeListener listener : mOnPositionChangeListeners) {
            listener.onDockChange(mDock.position());
        }
    }

    // This method is declared in this class simply to make it clear that its part of our public
    // contract and not just an inherited method.
    public void setOnClickListener(@Nullable Component.ClickedListener onClickListener) {
        super.setClickedListener(onClickListener);
    }

    public interface OnPositionChangeListener {
        void onPositionChange(@NonNull Point tabPosition);

        void onDockChange(@NonNull Point dockPosition);
    }
}
