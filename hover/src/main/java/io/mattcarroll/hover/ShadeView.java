/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * Fullscreen {@code View} that appears behind the other visual elements in a {@link HoverView} and
 * darkens the background.
 */
class ShadeView extends StackLayout {

    private static final int FADE_DURATION = 250;

    public ShadeView(@NonNull Context context) {
        this(context, null);
    }

    public ShadeView(@NonNull Context context, @Nullable AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_shade, this, true);
    }

    public void show() {
        AnimatorProperty fadeIn = new AnimatorProperty();
        fadeIn.setDuration(FADE_DURATION);
        fadeIn.alpha(1.0f);
        fadeIn.setTarget(this);
        fadeIn.start();

        setVisibility(VISIBLE);
    }

    public void showImmediate() {
        setVisibility(VISIBLE);
    }

    public void hide() {
        AnimatorProperty fadeOut = new AnimatorProperty();
        fadeOut.setDuration(FADE_DURATION);
        fadeOut.alphaFrom(1.0f);
        fadeOut.alpha(0.0f);
        fadeOut.setTarget(this);
        fadeOut.start();

        fadeOut.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                setVisibility(HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
    }

    public void hideImmediate() {
        setVisibility(HIDE);
    }
}
