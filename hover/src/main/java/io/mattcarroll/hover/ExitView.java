/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Point;
import ohos.app.Context;

import java.util.logging.Logger;

/**
 * Fullscreen View that provides an exit "drop zone" for users to exit the Hover Menu.
 */
public class ExitView extends DependentLayout {

    private static final String TAG = "ExitView";

    private int mExitRadiusInPx;
    private Component mExitIcon;

    public ExitView(@NonNull Context context) {
        this(context, null);
    }

    public ExitView(@NonNull Context context, @Nullable AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_hover_menu_exit,this,true);

        mExitIcon = findComponentById(ResourceTable.Id_view_exit);

        mExitRadiusInPx = 225;
    }

    public boolean isInExitZone(@NonNull Point position) {
        Point exitCenter = getExitZoneCenter();
        double distanceToExit = calculateDistance(position, exitCenter);
        Logger.getLogger(TAG).severe("Drop point: " + position + ", Exit center: " + exitCenter + ", Distance: " + distanceToExit);
        return distanceToExit <= mExitRadiusInPx;
    }

    private Point getExitZoneCenter() {
        int[] point = mExitIcon.getLocationOnScreen();

        return new Point(
                (int) (point[0] + (mExitIcon.getWidth() / 2)),
                (int) (point[1] + (mExitIcon.getHeight() / 2))
        );
    }

    private double calculateDistance(@NonNull Point p1, @NonNull Point p2) {
        return Math.sqrt(
                Math.pow(p2.getPointX() - p1.getPointX(), 2) + Math.pow(p2.getPointY() - p1.getPointY(), 2)
        );
    }
}
