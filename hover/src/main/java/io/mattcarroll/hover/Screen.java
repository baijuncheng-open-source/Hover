/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.mattcarroll.hover;


import io.mattcarroll.hover.AnnotationDef.NonNull;
import io.mattcarroll.hover.AnnotationDef.Nullable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static ohos.agp.components.Component.HIDE;


/**
 * The visual area occupied by a {@link HoverView}. A {@code Screen} acts as a factory for the
 * visual elements used within a {@code HoverView}.
 */
public class Screen {

    private static final String TAG = "Screen";

    private ComponentContainer mContainer;
    private ContentDisplay mContentDisplay;
    private ExitView mExitView;
    private ShadeView mShadeView;
    private Map<String, FloatingTab> mTabs = new HashMap<>();
    private boolean mIsDebugMode = false;

    Screen(@NonNull ComponentContainer hoverMenuContainer) {
        mContainer = hoverMenuContainer;

        mShadeView = new ShadeView(mContainer.getContext());
        mContainer.addComponent(mShadeView, new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT
        ));
        mShadeView.hideImmediate();

        mExitView = new ExitView(mContainer.getContext());
        mContainer.addComponent(mExitView, new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT
        ));
        mExitView.setVisibility(HIDE);

        mContentDisplay = new ContentDisplay(mContainer.getContext());
        mContainer.addComponent(mContentDisplay);
        mContentDisplay.setVisibility(HIDE);
    }

    public void enableDrugMode(boolean debugMode) {
        mIsDebugMode = debugMode;

        mContentDisplay.enableDebugMode(debugMode);
        for (FloatingTab tab : mTabs.values()) {
            //tab.enableDebugMode(debugMode);
        }
    }

    public int getWidth() {
        return mContainer.getWidth();
    }

    public int getHeight() {
        return mContainer.getHeight();
    }

    @NonNull
    public FloatingTab createChainedTab(@NonNull HoverMenu.SectionId sectionId, @NonNull Component tabView, boolean isExpend) {
        String tabId = sectionId.toString();
        return createChainedTab(tabId, tabView, isExpend);
    }

    @NonNull
    public FloatingTab createChainedTab(@NonNull String tabId, @NonNull Component tabView, boolean isExpend) {
        Logger.getLogger(TAG).severe("Existing tabs...");
        for (String existingTabId : mTabs.keySet()) {
            Logger.getLogger(TAG).severe(existingTabId);
        }
        if (mTabs.containsKey(tabId)) {
            return mTabs.get(tabId);
        } else {
            FloatingTab chainedTab = new FloatingTab(mContainer.getContext(), tabId, isExpend);
            chainedTab.setTabView(tabView);
            //chainedTab.enableDebugMode(mIsDebugMode);
            mContainer.addComponent(chainedTab);
            Logger.getLogger(TAG).severe("Creating new tab with ID=" + tabId + "+++  chainedTab=" + chainedTab.getEstimatedWidth() + "  chainedTab=" + chainedTab);
            mTabs.put(tabId, chainedTab);
            return chainedTab;
        }
    }

    @Nullable
    public FloatingTab getChainedTab(@Nullable HoverMenu.SectionId sectionId) {
        String tabId = null != sectionId ? sectionId.toString() : null;
        return getChainedTab(tabId);
    }

    @Nullable
    public FloatingTab getChainedTab(@Nullable String tabId) {
        return mTabs.get(tabId);
    }

    public void destroyChainedTab(@NonNull FloatingTab chainedTab) {
        mTabs.remove(chainedTab.getTabId());
        chainedTab.setTabView(null);
        mContainer.removeComponent(chainedTab);
    }

    public ContentDisplay getContentDisplay() {
        return mContentDisplay;
    }

    public ExitView getExitView() {
        return mExitView;
    }

    public ShadeView getShadeView() {
        return mShadeView;
    }

    public ComponentContainer getContainer() {
        return mContainer;
    }
}
