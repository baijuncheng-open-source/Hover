Hover
=====
Hover is a floating menu implementation for Ohos.

Goals
-----
The goals of Hover are to:

1. Provide an easy-to-use, out-of-the-box floating menu implementation for Ohos developers, and

1. Provide common tools for Ohos developers to create their own floating menu.

Beta Notice
-------
Hover is still under heavy development. There is still a lot of code cleanup to do, so expect breaking API changes over time.

That said, Hover should be in a usable state at this time.

There is still code to clean, but hopefully no further refactor of this scale will be necessary.

Demo Hover Menu
---------------
A demo app (called Kitchen Sink) is included with the Hover repo. Here are some screenshots of the demo in action.

- floating ball:

![img](/images/floating_ball.gif)

Getting Started
---------------
### Subclass HoverMenuService
To get started with Hover, create a subclass of `HoverMenuService` to host your Hover menu. Implement `onHoverMenuLaunched(Intent, HoverView)` to take control of your `HoverView`. You'll want to set it's `HoverMenu`, and also start it in the `collapsed` or `expanded` state:

```java
public class MyHoverMenuService extends HoverMenuService {

    @Override
    protected void onHoverMenuLaunched(@NonNull Intent intent, @NonNull HoverView hoverView) {
        // Configure and start your HoverView.
        HoverMenu menu = ...;
        hoverView.setMenu(menu);
        hoverView.collapse();
    }
    
}
```

### Implement A HoverMenu
A `HoverMenu` is the content that appears within a `HoverView`. A `HoverMenu` is divided into an ordered list of `Section`s.  Each `Section` has a tab as well as `Content` that appear in your `HoverView`.


```
/**
 * Demo implementation of a {@link HoverMenu}.
 */
public class DemoHoverMenu extends HoverMenu {

    public static final String INTRO_ID = "intro";
    public static final String SELECT_COLOR_ID = "select_color";
    public static final String APP_STATE_ID = "app_state";
    public static final String MENU_ID = "menu";
    public static final String PLACEHOLDER_ID = "placeholder";

    private final Context mContext;
    private final String mMenuId;
    private HoverTheme mTheme;
    private final List<Section> mSections = new ArrayList<>();

    public DemoHoverMenu(@NonNull Context context,
                         @NonNull String menuId,
                         @NonNull Map<String, Content> data,
                         @NonNull HoverTheme theme) {
        mContext = context;
        mMenuId = menuId;
        mTheme = theme;

        for (String tabId : data.keySet()) {
            mSections.add(new Section(
                    new SectionId(tabId),
                    createTabView(tabId),
                    data.get(tabId)
            ));
        }
    }

    public void setTheme(@NonNull HoverTheme theme) {
        mTheme = theme;
        // TODO: need to make theme changes work again with refactored menu
        //notifyMenuChanged();
    }

    Component createTabView(String sectionId) {
        if (INTRO_ID.equals(sectionId)) {
            return createTabView(ResourceTable.Media_ic_orange_circle, mTheme.getAccentColor(), null);
        } else if (SELECT_COLOR_ID.equals(sectionId)) {
            return createTabView(ResourceTable.Media_ic_paintbrush, mTheme.getAccentColor(), mTheme.getBaseColor());
        } else if (APP_STATE_ID.equals(sectionId)) {
            return createTabView(ResourceTable.Media_ic_stack, mTheme.getAccentColor(), mTheme.getBaseColor());
        } else if (MENU_ID.equals(sectionId)) {
            return createTabView(ResourceTable.Media_ic_menu, mTheme.getAccentColor(), mTheme.getBaseColor());
        } else if (PLACEHOLDER_ID.equals(sectionId)) {
            return createTabView(ResourceTable.Media_ic_pen, mTheme.getAccentColor(), mTheme.getBaseColor());
        } else {
            throw new RuntimeException("Unknown tab selected: " + sectionId);
        }
    }

    private Component createTabView(@DrawableRes int tabBitmapRes, @ColorInt int backgroundColor, @ColorInt Integer iconColor) {
        //Resources resources = mContext.getResources();
        //int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, resources.getDisplayMetrics());

        DemoTabView view = new DemoTabView(mContext, getPixelMapElement(ResourceTable.Media_tab_background), getPixelMapElement(tabBitmapRes));
        view.setTabBackgroundColor(backgroundColor);
        view.setTabForegroundColor(iconColor);
        return view;
    }

    private PixelMapElement getPixelMapElement(int resId) {
        return new PixelMapElement(getPixelMapFromResource(mContext, resId));
    }

    ...

    @Override
    public String getId() {
        return mMenuId;
    }

    @Override
    public int getSectionCount() {
        return mSections.size();
    }

    @Nullable
    @Override
    public Section getSection(int index) {
        return mSections.get(index);
    }

    @Nullable
    @Override
    public Section getSection(@NonNull SectionId sectionId) {
        for (Section section : mSections) {
            if (section.getId().equals(sectionId)) {
                return section;
            }
        }
        return null;
    }

    @NonNull
    @Override
    public List<Section> getSections() {
        return new ArrayList<>(mSections);
    }
}
```


### Working Directly With A HoverView
If you want to create your own Hover `Service` from scratch, or if you want to experiment with a `HoverView` directly, you can instantiate one yourself.


```
java
// Create a HoverView to display in a Window:
HoverView hoverView = HoverView.createForWindow(
        context,
        new WindowViewController(
            (WindowManager) getSystemService(Context.WINDOW_SERVICE)
        )
);
hoverView.setOnExitListener(onExitListener);
hoverView.addToWindow();
hoverView.setMenu(...);
hoverView.collapse();
 
// Create a HoverView in XML:
    <io.mattcarroll.hover.HoverView
        xmlns:hover="http://schemas.huawei.com/res/ohos-auto"
        ohos:id="$+id:hovermenu"
        ohos:width="match_content"
        ohos:height="match_content"
        hover:dockSide="right"
        hover:dockPosition="70%"
        />
```
### Usage

Add this in your root `build.gradle` file (**not** your module `build.gradle` file):

```xml
allprojects {
	repositories {
		...
		mavenCentral()
	}
}
```

The lib is available on Maven Central, you can find it with [Gradle, please]


```
The lib is available on Maven Central, you can find it with [Gradle, please]

dependencies {
    implementation ‘com.gitee.baijuncheng-open-source：hover：1.0.0’
}
```


### Issues

When Hover is used within a Window, there is always a fullscreen View - even when nothing is visible.  This is done to dramatically simplify the layout logic. However, this causes problems when apps request runtime permissions because the ohos OS complains that an overlay is visible.

There is no built-in solution for this problem at this time. You should take care to destroy your Hover `Service` when the `HoverView` is closed. You may also want to inform the users of your app that issues with runtime permission dialogs might occur, and that those users should exit your Hover menu if problems occur.

### defect
1.ohos.permission.SYSTEM_ FLOAT_ Windows permissions cannot be applied dynamically. You need to manually open the hover window permissions from Settings > Application > hover > permissions > hover window.

2. Because demo relies on the tripartite library com.la rswerkman:HoloColorPicker : 1.5, ohos can't use it, some floating menu pages can not be displayed, not related to the use of the library.

### Disclaimer

This is not an official ohos product.

### License

=======

    Copyright (C) 2016 Nest Labs

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
